﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InteractionDiffTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        string sfolder = "c:\\Projects\\Genesys\\Gencloud\\Technevalue\\Gitlab\\expressagent\\ExpressAgent\\bin\\Debug\\net6.0-windows\\events\\conversation";
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }

        List<string> conversations = new List<string>();

        bool bLoading = true;
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (var folderPath in Directory.GetDirectories(sfolder)) 
            { 

                string dirName = new DirectoryInfo(folderPath).Name;

                conversations.Add(dirName);
            }
            cboConversations.ItemsSource = conversations;
            cboConversations.SelectedIndex= 0;
            loadEvents();
            bLoading = false;
        }

        List<string> events = new List<string>();

        string Actfolder = "";
        void loadEvents() 
        {
            Actfolder = sfolder + "\\" + cboConversations.SelectedItem.ToString();
            foreach (var filePath in Directory.GetFiles(Actfolder))
            {
                string file = new FileInfo(filePath).Name;
                events.Add(file);
            }
            lstOne.ItemsSource = events;
            lstTwo.ItemsSource = events;
        }
        private void cboConversations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (bLoading == true) return;
            bLoading = true;
            loadEvents();
            bLoading = false;
        }


        string difftool2 = @"c:\Projects\Genesys\Gencloud\Technevalue\Gitlab\expressagent\Tools\windiff.exe";
        string difftool = "\"C:\\Program Files (x86)\\WinMerge\\WinMergeU.exe\"";
        Process actProcess = null;

        private void lstOne_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            int idx1 = lstOne.SelectedIndex;
            int idx2 = lstOne.SelectedIndex<lstOne.Items.Count-1 ? lstOne.SelectedIndex+1 : lstOne.SelectedIndex;


            string file1 = events[idx1];
            string file2 = events[idx2];
            file1 = Actfolder + "\\" + file1;
            file2 = Actfolder+"\\" +file2;

            if (bLoading == true) return;
            if (actProcess != null) 
            {
                actProcess.Kill();
            }
            actProcess = new System.Diagnostics.Process();
            actProcess.StartInfo.FileName = difftool2;
            actProcess.StartInfo.Arguments = "\"" + file1+ "\" \"" + file2 +"\"";
            //actProcess.StartInfo.RedirectStandardOutput = true;
            //actProcess.StartInfo.UseShellExecute = false;
            //actProcess.StartInfo.CreateNoWindow = true;
            actProcess.Start();
        }

        private void lstTwo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
