﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;

namespace ExpressAgent.Platform.Abstracts
{
    public abstract class PlatformServiceBase<T> : INotifyPropertyChanged
    {
        internal T ApiInstance;
        internal AgentSession agentSession;

        public PlatformServiceBase(T apiInstance, AgentSession session)
        {
            ApiInstance = apiInstance;
            agentSession = session;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        Dictionary<string, int> loggedEvents = new Dictionary<string, int>();

        public void SaveToFilesystem(string subdir, string id, string json) 
        {
            try
            {
                var currdir = Directory.GetCurrentDirectory();

                string eventsdir = currdir + "\\events";
                if (!Directory.Exists(eventsdir)) Directory.CreateDirectory(eventsdir);

                string convdir = eventsdir + "\\" + subdir;
                if (!Directory.Exists(convdir)) Directory.CreateDirectory(convdir);


                int event_number = 1;
                string event_id = id;
                lock (loggedEvents)
                {
                    if (!loggedEvents.ContainsKey(event_id))
                    {
                        loggedEvents.Add(event_id, event_number);
                    }
                    else
                    {
                        loggedEvents[event_id]++;
                        event_number = loggedEvents[event_id];
                    }
                }

                File.WriteAllText(convdir + "\\" + String.Format("{0000}", event_number) + ".json", json);

            }
            catch (Exception ex)
            {
                //("ConversationSrv_OnConversationEvent error:" + ex);
            }
        }
    }
}
