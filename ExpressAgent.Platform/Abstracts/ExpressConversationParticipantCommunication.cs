﻿using ExpressAgent.Platform.Models;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using static PureCloudPlatform.Client.V2.Model.Call;

namespace ExpressAgent.Platform.Abstracts
{
    public abstract class ExpressConversationParticipantCommunication : INotifyPropertyChanged
    {
        protected NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();
        public ExpressConversationParticipantCommunication(ExpressConversationParticipant participant)
        {
            _Participant = participant;
        }


        //enabled bindings:
        public bool IsPickupEnabled { get { return _State == "Alerting" ? true : false;   } set { } }
        public bool IsDisconnectEnabled { get { return (_State == "Alerting" || _State == "Connected" || _State == "Contacting" || _State == "Dialing" || _State == "Offering") ? true : false;   } set { } }
        public bool IsMuteHoldRecordEnabled { get { return _State == "Connected" ? true : false;  } set { } }
        public bool IsSecurePauseEnabled { get { return ((_State == "Connected") && (Recording == true)) ? true : false;   } set { } }
        public bool SecurePaused { get { return RecordingState == "Paused" ? true : false; } set { } }
        public bool IsFlagEnabled { get { return (_State == "Connected") ? true : false; } set { } }

        public System.Windows.Visibility IsWrapupVisible { get { return _State == "Disconnected" || _State == "Terminated" ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;  } set { } }



        private string _Id;
        private string _State;
        private string _RecordingState; //securepause: None/Active/Paused
        private string _Direction;

        private bool? _Held;
        private bool? _Recording;
        private bool? _Confined;//not used
        private bool _Flagged; //calculated from Participant.Conversations.Flaggedreason
        private bool? _AfterCallWorkRequired;

        private DateTime? _ConnectedTime;
        private ExpressConversationParticipant _Participant;
        public string Id
        {
            get
            {
                return _Id;
            }
            set
            {
                if (value != _Id)
                {
                    _Id = value;
                    OnPropertyChanged();
                }
            }
        }
        public string State
        {
            get
            {
                return _State;
            }
            set
            {
                if (value != _State)
                {
                    _State = value;
                    OnPropertyChanged();
                    OnPropertyChanged("IsPickupEnabled");
                    OnPropertyChanged("IsDisconnectEnabled");
                    OnPropertyChanged("IsMuteHoldRecordEnabled");
                    OnPropertyChanged("IsSecurePauseEnabled");
                    OnPropertyChanged("IsFlagEnabled");
                    OnPropertyChanged("IsWrapupVisible");
                }
            }
        }
        public string RecordingState
        {
            get
            {
                return _RecordingState;
            }
            set
            {
                if (value != _RecordingState)
                {
                    _RecordingState = value;
                    OnPropertyChanged();
                    OnPropertyChanged("SecurePaused");
                }
            }
        }
        public string Direction
        {
            get
            {
                return _Direction;
            }
            set
            {
                if (value != _Direction)
                {
                    _Direction  = value;
                    OnPropertyChanged();
                }
            }
        }
        public bool? Held
        {
            get
            {
                return _Held;
            }
            set
            {
                if (value != _Held)
                {
                    _Held = value;
                    OnPropertyChanged();
                }
            }
        }
        public bool? Confined
        {
            get
            {
                return _Confined;
            }
            set
            {
                if (value != _Confined)
                {
                    _Confined = value;
                    OnPropertyChanged();
                }
            }
        }
     
        public bool? Recording
        {
            get
            {
                return _Recording;
            }
            set
            {
                if (value != _Recording)
                {
                    _Recording = value;
                    OnPropertyChanged();
                    OnPropertyChanged("IsSecurePauseEnabled");

                }
            }
        }

        public bool? AfterCallWorkRequired
        {
            get
            {
                return _AfterCallWorkRequired;
            }
            set
            {
                if (value != _AfterCallWorkRequired)
                {
                    _AfterCallWorkRequired = value;
                    OnPropertyChanged();
                }
            }
        }
        

        public bool Flagged
        {
            get
            {
                return _Flagged;
            }
            set
            {
                if (value != _Flagged)
                {
                    _Flagged = value;
                    OnPropertyChanged();
                }
            }
        }
        public DateTime? ConnectedTime
        {
            get
            {
                return _ConnectedTime;
            }
            set
            {
                if (value != _ConnectedTime)
                {
                    _ConnectedTime = value;
                    OnPropertyChanged();
                }
            }
        }
        public ExpressConversationParticipant Participant
        {
            get
            {
                return _Participant;
            }
        }

        #region Toggles
        public void Pickup()
        {
            if (Participant == null)
            {
                Log.Debug($"ExpressConversationParticipantCommunication: Unable to pickup due to missing participant on {Id}");
                return;
            }

            MediaParticipantRequest body = new MediaParticipantRequest()
            {
                State = MediaParticipantRequest.StateEnum.Connected
            };

            Participant.Conversation.ConversationService.UpdateParticipant(Participant.Conversation.Id, Participant.Id, body);
        }

        public void Disconnect()
        {
            if (Participant == null)
            {
                Log.Debug($"ExpressConversationParticipantCommunication: Unable to disconnect due to missing participant on {Id}");
                return;
            }

            MediaParticipantRequest body = new MediaParticipantRequest()
            {
                State = MediaParticipantRequest.StateEnum.Disconnected
            };

            Participant.Conversation.ConversationService.UpdateParticipant(Participant.Conversation.Id, Participant.Id, body);
        }

        public void ToggleRecording()
        {
            if (Participant == null)
            {
                Log.Debug($"ExpressConversationParticipantCommunication: Unable to toggle record state due to missing participant on {Id}");
                return;
            }

            MediaParticipantRequest body = new MediaParticipantRequest()
            {
                Recording = !Recording ?? true
            };

            Participant.Conversation.ConversationService.UpdateParticipant(Participant.Conversation.Id, Participant.Id, body);
        }

        public void ToggleHold()
        {
            if (Participant == null)
            {
                Log.Debug($"ExpressConversationParticipantCommunication: Unable to toggle hold state due to missing participant on {Id}");
                return;
            }

            MediaParticipantRequest body = new MediaParticipantRequest()
            {
                Held = !Held ?? true
            };

            Participant.Conversation.ConversationService.UpdateParticipant(Participant.Conversation.Id, Participant.Id, body);
        }

        public void ToggleFlagged()
        {
            if (Participant == null)
            {
                Log.Debug($"ExpressConversationParticipantCommunication: Unable to toggle flag state due to missing participant on {Id}");
                return;
            }
            Participant.Conversation.ConversationService.UpdateParticipantFlagged(Participant.Conversation.Id, Participant.Id, !Flagged);
        }

        public void ToggleSecurePaused()
        {
            if (Participant == null)
            {
                Log.Debug($"ExpressConversationParticipantCommunication: Unable to toggle SecurePaused due to missing participant on {Id}");
                return;
            }
            Participant.Conversation.ConversationService.UpdateCallRecordingState(Participant.Conversation.Id, !SecurePaused);
        }


        public void ToggleAftercallwork() 
        {
            if (Participant == null)
            {
                Log.Debug($"ExpressConversationParticipantCommunication: Unable to disconnect due to missing participant on {Id}");
                return;
            }

            AfterCallWorkUpdate body = new AfterCallWorkUpdate() 
            { 
                AfterCallWorkRequired = !AfterCallWorkRequired ?? true
            };
            Participant.Conversation.ConversationService.RequestAftercallwork(Participant.Conversation.Id, Participant.Id, Participant.Communications[0].Id, body);
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
