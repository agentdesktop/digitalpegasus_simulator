﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Resources;
using System.Xml.Linq;

namespace ExpressAgent.Platform
{
    public class DisplayLanguage
    {
        public string DisplayName { get; set; }
        public string LanguageCode { get; set; }
    }
    public class Languages : INotifyPropertyChanged
    {
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        #region Instance
        private static Languages _GlobalInstance = null;
        public static Languages GetInstance()
        {
            if (_GlobalInstance == null)
            {
                _GlobalInstance = new Languages();
            }
            return _GlobalInstance;
        }
        #endregion

        #region Init
        public readonly string DefaultLangCode = "en";
        public List<DisplayLanguage> _DisplayLanguages = null;
        public List<DisplayLanguage> DisplayLanguages { get { return _DisplayLanguages; } }
        private XDocument LanguageDoc;
        private Dictionary<string, string> LanguageDict = new Dictionary<string, string>();
        public Languages()
        {
            LoadDisplayLanguages();
            LoadLanguage(DefaultLangCode);

        }

        private void LoadDisplayLanguages()
        {
            try
            {
                Uri uri = new Uri("Languages/Languages.xml", UriKind.Relative);
                StreamResourceInfo info = Application.GetResourceStream(uri);
                LanguageDoc = XDocument.Load(info.Stream);
                _DisplayLanguages = new List<DisplayLanguage>();
                var q = from c in LanguageDoc.Descendants("Language") select new DisplayLanguage() { DisplayName = (string)c.Attribute("DisplayName"), LanguageCode = (string)c.Attribute("Code") };
                foreach (var displayLanguage in q)
                {
                    _DisplayLanguages.Add(displayLanguage);
                }
            }
            catch (Exception ex)
            {
                Log.Error("LoadDisplayLanguages exception:" + ex);
            }
        }
        public void RaiseLanguageChanged()
        {
            if (LanguageChanged != null)
            {
                LanguageChanged(sLoadedLanguageCode);
            }
        }

        private string sLoadedLanguageCode = "";
        public void LoadLanguage(string sLanguageCode)
        {
            try
            {
                if (sLoadedLanguageCode == sLanguageCode) return;
                sLoadedLanguageCode = sLanguageCode;

                LanguageDict = new Dictionary<string, string>();
                var actNode = from c in LanguageDoc.Descendants("Language") where (string)c.Attribute("Code") == sLanguageCode select c;
                foreach (var xel in actNode.Descendants())
                {
                    try
                    {
                        //LanguageDict.Add(xel.Name.ToString(), xel.Attribute("Value").Value.ToString());
                        this[xel.Name.ToString()] = xel.Attribute("Value").Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        Log.Error("LoadLanguage loop exception:" + ex);

                    }
                }

                //raise all binding update to WPF
                RaiseLanguageEmptyPropertyChanged();
                //raise LanguageChanged event
                RaiseLanguageChanged();
            }
            catch (Exception ex)
            {
                Log.Error("LoadLanguage exception:" + ex);
            }
        }
        #endregion


        public bool ContainsKey(string property)
        {
            return LanguageDict.ContainsKey(property);
        }
        public string this[string property]
        {
            get
            {
                if (!LanguageDict.ContainsKey(property)) return null;
                return LanguageDict[property];
            }
            set
            {
                if (!LanguageDict.ContainsKey(property))
                    LanguageDict.Add(property, value.ToString());
                else
                    LanguageDict[property] = value.ToString();
                RaiseLanguagePropertyChanged(property);
            }
        }

        #region Events
        private void RaiseLanguagePropertyChanged(string propertyName)
        {
            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(string.Format("Item[{0}]", propertyName)));
        }
        private void RaiseLanguageEmptyPropertyChanged()
        {
            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(string.Empty));
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public event LanguageChangedEventHandler LanguageChanged;
        public delegate void LanguageChangedEventHandler(string sLanguageCode);

        #endregion
    }
}