﻿using ExpressAgent.Platform.Abstracts;
using ExpressAgent.Platform.Enums;
using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Client;
using PureCloudPlatform.Client.V2.Extensions.Notifications;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace ExpressAgent.Platform.Services
{
    public class UserService : PlatformServiceBase<UsersApi>, INotifyPropertyChanged
    {
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();
        public UserService(UsersApi apiInstance, AgentSession session) : base(apiInstance, session)
        {
        }

        #region binded props
        private RoutingState _CurrentRoutingState;
        public RoutingState CurrentRoutingState
        {
            get
            {
                return _CurrentRoutingState;
            }
            private set
            {
                if (value != _CurrentRoutingState)
                {
                    _CurrentRoutingState = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion

        public ObservableCollection<User> Users { get; set; }



        #region Notification handlers
        public void HandleRoutingStatusEvent(NotificationData<UserRoutingStatusUserRoutingStatus> routingStatusEvent)
        {
            Log.Debug($"UserService: Routing status event received: New routing status is {routingStatusEvent.EventBody.RoutingStatus.Status}");

            CurrentRoutingState = FromUserRoutingStatus(routingStatusEvent.EventBody.RoutingStatus);
        }
        #endregion

        //AgentSession.CurrentUser
        public UserMe GetCurrentUser()
        {
            try
            {
                Log.Debug($"UserService: Calling GetUsersMe");

                return ApiInstance.GetUsersMe(new List<string> { "presence", "routingStatus", "station" });
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

            return new UserMe();
        }


        //called by Session_Authenticated
        public ObservableCollection<User> SetUserCollection()
        {
            try
            {
                Users = new ObservableCollection<User>(GetUsers());
                return Users;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

            return new ObservableCollection<User>();
        }

        private List<User> GetUsers(int pageNumber = 0)
        {
            try
            {
                Log.Debug($"RoutingService: Calling GetRoutingQueues");

                UserEntityListing result = ApiInstance.GetUsers(100,pageNumber);
                List<User> users = new List<User>();
                foreach (var entity in result.Entities)
                {
                    users.Add(entity);
                }

                if (result.PageNumber != null)
                {
                    if (result.PageCount > result.PageNumber)
                    {
                        users.AddRange(GetUsers((int)result.PageNumber + 1));
                    }
                }

                return users;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

            return new List<User>();
        }
    

        //unused
        public RoutingState GetUserRoutingState(string userId)
        {
            try
            {
                Log.Debug($"UserService: Calling GetUserRoutingstatus");

                RoutingStatus routingStatus = ApiInstance.GetUserRoutingstatus(userId);

                return FromUserRoutingStatus(routingStatus);
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

            return RoutingState.OffQueue;
        }

        //called from NotResponding.OnQueueButton
        public bool SetUserIdle()
        {
            try
            {
                RoutingStatus body = new RoutingStatus
                {
                    Status = RoutingStatus.StatusEnum.Idle
                };

                Log.Debug($"PresenceService: Calling PutUserRoutingstatus");

                RoutingStatus routingStatus = ApiInstance.PutUserRoutingstatus(agentSession.CurrentUser.Id, body);

                return routingStatus.Status == RoutingStatus.StatusEnum.Idle;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

            return false;
        }

        //called from QueueActivation
        public UserQueue ActivateMyQueue(UserQueue queue, bool activate = true)
        {
            try
            {

                Log.Debug($"RoutingService: ActivateMyQueue");
                UserQueue body = new UserQueue() { Joined = activate };
                UserQueue result = ApiInstance.PatchUserQueue(queue.Id, agentSession.CurrentUser.Id, body);
                return result;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
                return null;
            }
        }


        public List<User> SearchTargets(string filter)
        {

            List<UserSearchCriteria>  QueryList = new List<UserSearchCriteria>() { new UserSearchCriteria() { Value = filter, Type = UserSearchCriteria.TypeEnum.QueryString } };
            List<string> expand = new List<string>() { "presence", "conversationSummary", "outOfOffice" };// "authorization",    "routingStatus",
            UserSearchRequest body = new UserSearchRequest()
            {
                Query = QueryList,
                SortOrder = UserSearchRequest.SortOrderEnum.Asc,
                SortBy = "name",
                Expand = expand
            };
            UsersSearchResponse response = ApiInstance.PostUsersSearchConversationTarget(body);

            List<User> users = response.Results;
            if (users != null)
            {
                //remove current user from list
                var myUser = users.Where(x => x.Id == agentSession?.CurrentUser.Id).FirstOrDefault();
                if (myUser != null)
                {
                    users.Remove(myUser);
                }
                //update presence names
                foreach (var user in users)
                {
                    var expresence = agentSession?.PresenceSrv.OrgPresences.Where(x => x.Id == user.Presence.PresenceDefinition.Id).FirstOrDefault();
                    if (expresence != null)
                    {
                        user.Presence.Name = expresence.Name;
                    }
                }
            }
            return users;


        }

        #region Converters
        private RoutingState FromUserRoutingStatus(RoutingStatus routingStatus)
        {
            switch (routingStatus.Status)
            {
                case RoutingStatus.StatusEnum.NotResponding:
                    return RoutingState.NotResponding;
                case RoutingStatus.StatusEnum.Idle:
                case RoutingStatus.StatusEnum.Interacting:
                case RoutingStatus.StatusEnum.Communicating:
                    return RoutingState.OnQueue;
                default:
                    return RoutingState.OffQueue;
            }
        }

        private RoutingState FromUserRoutingStatus(UserRoutingStatusRoutingStatus routingStatus)
        {
            switch (routingStatus.Status)
            {
                case UserRoutingStatusRoutingStatus.StatusEnum.NotResponding:
                    return RoutingState.NotResponding;
                case UserRoutingStatusRoutingStatus.StatusEnum.Idle:
                case UserRoutingStatusRoutingStatus.StatusEnum.Interacting:
                case UserRoutingStatusRoutingStatus.StatusEnum.Communicating:
                    return RoutingState.OnQueue;
                default:
                    return RoutingState.OffQueue;
            }
        }

        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
