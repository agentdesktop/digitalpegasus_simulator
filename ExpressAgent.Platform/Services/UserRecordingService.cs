﻿using ExpressAgent.Notifications;
using ExpressAgent.Platform.Abstracts;
using ExpressAgent.Platform.Models;
using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Extensions.Notifications;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;

namespace ExpressAgent.Platform.Services
{
    public class UserRecordingsService : PlatformServiceBase<UserRecordingsApi>, INotifyPropertyChanged
    {
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();
        public UserRecordingsService(UserRecordingsApi apiInstance, AgentSession session) : base(apiInstance, session)
        {
            UserRecordings = new ObservableCollection<ExpressUserRecording>();
        }

        public ObservableCollection<ExpressUserRecording> UserRecordings { get; set; }


        int _UnReadUserRecordingCount = 0;
        public int UnReadUserRecordingCount
        {
            get { return _UnReadUserRecordingCount; }
            set
            {
                if (_UnReadUserRecordingCount != value) { _UnReadUserRecordingCount = value; OnPropertyChanged("UnReadUserRecordingCount"); }
            }
        }

        public void UpdateUnReadUserRecordingsCount()
        {
            int count = UserRecordings.Where(x => x.Read == false).ToList().Count;
            UnReadUserRecordingCount = count;
        }


        async public void SetUserRecordings()
        {
            List<UserRecording> vms = null;
            await Task.Run(() =>
            {
                vms = GetUserRecordings();
            });

            UserRecordings.Clear();
            foreach (var msg in vms)//.Where(x => x.Deleted == false))
            {
                var expVM = new ExpressUserRecording(msg.Id);
                FromUserRecording(ref expVM, msg);
                UserRecordings.Add(expVM);
            }
            UpdateUnReadUserRecordingsCount();
        }

        void FromUserRecording(ref ExpressUserRecording vm, UserRecording msg)
        {
            vm.Read = msg.Read;
            vm.AudioRecordingDurationSeconds = msg.DurationMilliseconds;
            vm.CreatedDate = msg.DateCreated;
            vm.Name = msg.Name;
            vm.Conversation = msg.Conversation;
        }

        void FromUserRecordingNotification(ref ExpressUserRecording vm, AdhocRecordingTopicRecordingDataV2 msg)
        {
            vm.Read = msg.Read;
            vm.AudioRecordingDurationSeconds = msg.DurationMillieconds;
            vm.CreatedDate = msg.DateCreated;
            vm.Name=msg.Name;
            //vm.Conversation = msg.;
            //vm.CallerUser = msg.CallerUser;
            //vm.Queue = msg.Queue;
        }//*/


        public List<UserRecording> GetUserRecordings()
        {
            //UserRecordingMailboxInfo info = ApiInstance.GetUserRecordingMeMailbox();
            //info.TotalCount + info.DeletedCount + info.UnreadCount + info.UsageSizeBytes
            //UserRecordingUserPolicy policy = ApiInstance.GetUserRecordingMePolicy();
            UserRecordingEntityListing msgs = ApiInstance.GetUserrecordings();
            return msgs.Entities;
        }


        public DownloadResponse GetUserRecordingMedia(string Id)
        {
            DownloadResponse info = ApiInstance.GetUserrecordingMedia(Id);
            return info;
        }

        public void MarkUserRecordingRead(string Id, bool bRead = false)
        {
            ApiInstance.PutUserrecording(Id, body: new UserRecording() { Read = bRead });
        }

        public void DeleteUserRecording(string Id)
        {
            //ApiInstance.PutUserrecording(Id, body: new UserRecording() { Deleted = true });
        }

                        //"v2.users.{id}.userrecordings",typeof (AdhocRecordingTopicRecordingDataV2)

        #region Notification handlers
        public void HandleUserRecordingEvent(NotificationData<AdhocRecordingTopicRecordingDataV2> UserRecordingEvent)
        {
            Log.Debug($"UserRecordingsService: UserRecording event received: {UserRecordingEvent.EventBody}");

            //CurrentRoutingState = FromUserRoutingStatus(routingStatusEvent.EventBody.RoutingStatus);

            ExpressUserRecording expVM = UserRecordings.Where(c => c.Id == UserRecordingEvent.EventBody.Id).FirstOrDefault();
            /*if (UserRecordingEvent.EventBody.Deleted == true)
            {
                //delete
                if (expVM != null)
                {
                    UserRecordings.Remove(expVM);
                    UpdateUnReadUserRecordingsCount();
                }
            }
            else//*/
            {
                bool isExisting = false;
                if (expVM != null)
                {
                    isExisting = true;
                }
                else
                {
                    expVM = new ExpressUserRecording(UserRecordingEvent.EventBody.Id); //just add the id
                }
                FromUserRecordingNotification(ref expVM, UserRecordingEvent.EventBody);
                if (!isExisting)
                {
                    UserRecordings.Add(expVM);
                }
                UpdateUnReadUserRecordingsCount();
            }
        }

        #endregion
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}