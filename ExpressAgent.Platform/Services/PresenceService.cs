﻿using ExpressAgent.Platform.Abstracts;
using ExpressAgent.Platform.Models;
using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Client;
using PureCloudPlatform.Client.V2.Extensions.Notifications;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

namespace ExpressAgent.Platform.Services
{
    public class PresenceService : PlatformServiceBase<PresenceApi>, INotifyPropertyChanged
    {
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();
        public PresenceService(PresenceApi apiInstance, AgentSession session) : base(apiInstance, session)
        {
        }

        #region Binded props
        private ObservableCollection<ExpressPresence> _OrgPresences;
        public ObservableCollection<ExpressPresence> OrgPresences
        {
            get
            {
                if (_OrgPresences == null)
                {
                    _OrgPresences = new ObservableCollection<ExpressPresence>(GetOrgPresences());
                }

                return _OrgPresences;
            }
        }

        private ObservableCollection<ExpressPresence> _SelectebleOrgPresences;

        public ObservableCollection<ExpressPresence> SelectebleOrgPresences
        {
            get
            {
                if (_SelectebleOrgPresences == null)
                {
                    _SelectebleOrgPresences = new ObservableCollection<ExpressPresence>();
                    foreach (var presence in _OrgPresences.OrderBy(x => x.SystemPresence))
                    {
                        if (presence.SystemPresence != "Offline" && presence.SystemPresence != "Idle") 
                        {
                            _SelectebleOrgPresences.Add(presence);
                        }
                    }
                    //move onqueue first
                    var onQueue = _SelectebleOrgPresences.Where(x => x.SystemPresence == "On Queue" && x.Primary==true).FirstOrDefault();
                    if (onQueue != null) 
                    {
                        _SelectebleOrgPresences.Move(_SelectebleOrgPresences.IndexOf(onQueue), 0);
                    }
                    //move busy second
                    var busy = _SelectebleOrgPresences.Where(x => x.SystemPresence == "Busy" && x.Primary == true).FirstOrDefault();
                    if (busy != null)
                    {
                        _SelectebleOrgPresences.Move(_SelectebleOrgPresences.IndexOf(busy), 2);
                    }
                }
                return _SelectebleOrgPresences;
            }
        }

        private ExpressPresence _CurrentPresence;
        public ExpressPresence CurrentPresence
        {
            get
            {
                if (_CurrentPresence == null)
                {
                    _CurrentPresence = GetUserPresence(agentSession.CurrentUser.Id);
                }

                return _CurrentPresence;
            }
            private set
            {
                if (value != _CurrentPresence)
                {
                    _CurrentPresence = value;
                    OnPropertyChanged();
                }
            }
        }

        #endregion

        //recoursive call
        public List<ExpressPresence> GetOrgPresences(int pageNumber = 0)
        {
            try
            {
                Log.Debug($"PresenceService: Calling GetPresencedefinitions");

                OrganizationPresenceEntityListing result = ApiInstance.GetPresencedefinitions(pageNumber,25, "false", "en");
                List<ExpressPresence> presences = new List<ExpressPresence>();

                foreach(OrganizationPresence presence in result.Entities)
                {
                    presences.Add(FromOrgPresence(presence));
                }

                if (result.PageNumber != null)
                {
                    if (result.PageCount > result.PageNumber)
                    {
                        presences.AddRange(GetOrgPresences((int)result.PageNumber + 1));
                    }
                }

                return presences;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

            return new List<ExpressPresence>();
        }

        public ExpressPresence GetUserPresence(string userId)
        {
            try
            {
                Log.Debug($"PresenceService: Calling GetUserPresencesPurecloud");

                UserPresence userPresence = ApiInstance.GetUserPresencesPurecloud(userId);

                return FromUserPresence(userPresence);
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
                return new ExpressPresence();
            }

        }

        public bool SetUserPresence(string presenceId)//, string message = null)
        {
            if (CurrentPresence.Id == presenceId)// && (message ?? "") == (CurrentPresence.Message ?? ""))
            {
                return false;
            }

            try
            {
                UserPresence body = new UserPresence
                {
                    PresenceDefinition = new PresenceDefinition
                    {
                        Id = presenceId
                    }
                    //,Message = message
                };

                Log.Debug($"PresenceService: Calling PatchUserPresencesPurecloud");

                UserPresence userPresence = ApiInstance.PatchUserPresencesPurecloud(agentSession.CurrentUser.Id, body);

                return userPresence.PresenceDefinition.Id == presenceId;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

            return false;
        }

        public bool SetInitialPresence()
        {
            if (_OrgPresences == null)
            {
                _OrgPresences = new ObservableCollection<ExpressPresence>(GetOrgPresences());
            }

            string actPresenceId = agentSession.CurrentUser.Presence.PresenceDefinition.Id;
            

            if (_CurrentPresence == null)
            {
                _CurrentPresence = GetUserPresence(agentSession.CurrentUser.Id);
            }


            ExpressPresence availablePresence = OrgPresences.Where(p => p.SystemPresence == "Available" && p.Primary == true).FirstOrDefault();
            ExpressPresence offlinePresence = OrgPresences.Where(p => p.SystemPresence == "Offline" && p.Primary == true).FirstOrDefault();


            if (CurrentPresence.Id == offlinePresence.Id)
            {
                return SetUserPresence(availablePresence.Id);
            }
            else
            {
                return false;
            }
        }

        #region Conversion
        public ExpressPresence FromUserPresence(UserPresence userPresence)
        {
            ExpressPresence orgPresence = OrgPresences.Where(p => p.Id == userPresence.PresenceDefinition.Id).FirstOrDefault();

            return new ExpressPresence
            {
                Id = orgPresence.Id,
                Name = orgPresence.Name,
                //Message = userPresence.Message,
                SystemPresence = orgPresence.SystemPresence,
                Primary = orgPresence.Primary,
                ModifiedDate = DateTime.UtcNow
            };
        }

        public ExpressPresence FromOrgPresence(OrganizationPresence orgPresence)
        {
            return new ExpressPresence
            {
                Id = orgPresence.Id,
                Name = GetPresenceName(orgPresence),
                SystemPresence = orgPresence.SystemPresence,
                Primary = orgPresence.Primary ?? false
            };
        }

        private string GetPresenceName(OrganizationPresence presence,string language = "en")
        {
            string name = presence.Name;

            if (string.IsNullOrEmpty(presence.Name) && presence.LanguageLabels != null && presence.LanguageLabels.ContainsKey(language))
            {
                name = presence.LanguageLabels[language];
            }
            else if (string.IsNullOrEmpty(name))
            {
                name = presence.SystemPresence;
            }

            return name;
        }
        #endregion

        #region Notification handlers
        public void HandlePresenceEvent(NotificationData<PresenceEventUserPresence> presenceEvent)
        {
            if (presenceEvent.EventBody.Source != "PURECLOUD")
            {
                Log.Debug($"PresenceService: Ignoring presence update from unknown source {presenceEvent.EventBody.Source}");
                return;
            }

            // because the presence name doesn't come through in the notification, match up with our presence list using the ID
            ExpressPresence orgPresence = OrgPresences.Where(p => p.Id == presenceEvent.EventBody.PresenceDefinition.Id).FirstOrDefault();

            Log.Debug($"PresenceService: Presence event received: New presence is {orgPresence.Name} Message: {presenceEvent.EventBody.Message}");

            if (orgPresence.Id == CurrentPresence.Id)
            {
                // same presence, just a new message
                //CurrentPresence.Message = presenceEvent.EventBody.Message;
            }
            else
            {
                CurrentPresence = new ExpressPresence
                {
                    Id = orgPresence.Id,
                    Name = orgPresence.Name,
                    //Message = presenceEvent.EventBody.Message,
                    SystemPresence = orgPresence.SystemPresence,
                    Primary = orgPresence.Primary,
                    ModifiedDate = presenceEvent.EventBody.ModifiedDate
                };
            }
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
