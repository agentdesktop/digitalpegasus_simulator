﻿using ExpressAgent.Platform.Abstracts;
using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Client;
using PureCloudPlatform.Client.V2.Model;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Queue = PureCloudPlatform.Client.V2.Model.Queue;

namespace ExpressAgent.Platform.Services
{
    public class RoutingService : PlatformServiceBase<RoutingApi>
    {
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();
        public RoutingService(RoutingApi apiInstance, AgentSession session) : base(apiInstance, session)
        {
            Queues = new ObservableCollection<Queue>();
        }
        #region Binded props
        public ObservableCollection<Queue> Queues { get; set; }
        #endregion Binded props



        private List<Queue> GetQueues(int pageNumber = 0)
        {
            try
            {
                Log.Debug($"RoutingService: Calling GetRoutingQueues");

                QueueEntityListing result = ApiInstance.GetRoutingQueues(pageNumber);
                List<Queue> queues = new List<Queue>();


                foreach (var entity in result.Entities)
                {
                    queues.Add(entity);
                }

                if (result.PageNumber != null)
                {
                    if (result.PageCount > result.PageNumber)
                    {
                        queues.AddRange(GetQueues((int)result.PageNumber + 1));
                    }
                }

                return queues;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

            return new List<Queue>();
        }

        //called by Session_Authenticated
        public ObservableCollection<Queue> SetQueueCollection()
        {
            try
            {
                Queues = new ObservableCollection<Queue>(GetQueues());

                return Queues;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

            return new ObservableCollection<Queue>();
        }


        //called by QueueActivation
        public List<UserQueue> GetMyQueues(int? pageNumber = 0,bool? joined = true) 
        {
            List<UserQueue> queues = new List<UserQueue>();
            try
            {
                Log.Debug($"RoutingService: Calling GetRoutingQueuesMe");

                //UserQueueEntityListing result = ApiInstance.GetRoutingQueuesMe(pageNumber,50,joined);
                UserQueueEntityListing result = ApiInstance.GetRoutingQueuesMe();
                foreach (var entity in result.Entities)
                {
                    queues.Add(entity);
                }

                if (result.PageNumber != null)
                {
                    if (result.PageCount > result.PageNumber)
                    {
                        queues.AddRange(GetMyQueues((int)result.PageNumber + 1, joined));
                    }
                }

            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }
            return queues;

        }

        //GET https://apps.mypurecloud.de/platform/api/v2/routing/queues/divisionviews?name=g
        public List<Queue> SearchTargets(string queuename)
        {
            try
            {
                Log.Debug($"RoutingService: Calling SearchTargets");

                QueueEntityListing result = ApiInstance.GetRoutingQueuesDivisionviews(null, null, null, null, queuename);
                List<Queue> queues = new List<Queue>();


                foreach (var entity in result.Entities)
                {
                    queues.Add(entity);
                }

                /*
                if (result.PageNumber != null)
                {
                    if (result.PageCount > result.PageNumber)
                    {
                        queues.AddRange(GetQueues((int)result.PageNumber + 1));
                    }
                }//*/

                return queues;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

            return new List<Queue>();
        }


    }
}
