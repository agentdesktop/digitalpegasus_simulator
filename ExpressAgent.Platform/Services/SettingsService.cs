﻿using ExpressAgent.Notifications;
using ExpressAgent.Platform.Abstracts;
using ExpressAgent.Platform.Models;
using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Extensions.Notifications;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;

namespace ExpressAgent.Platform.Services
{
    public class SettingsService : PlatformServiceBase<SettingsApi>, INotifyPropertyChanged
    {
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();
        public SettingsService(SettingsApi apiInstance, AgentSession session) : base(apiInstance, session)
        {
        }

 

        public void EmailsSettings()
        {
            //VoicemailMailboxInfo info = ApiInstance.GetVoicemailMeMailbox();
            //info.TotalCount + info.DeletedCount + info.UnreadCount + info.UsageSizeBytes
            //VoicemailUserPolicy policy = ApiInstance.GetVoicemailMePolicy();
            var msgs = ApiInstance.GetEmailsSettings();
            //return msgs;
        }


        
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}