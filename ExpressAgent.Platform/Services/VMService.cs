﻿using ExpressAgent.Notifications;
using ExpressAgent.Platform.Abstracts;
using ExpressAgent.Platform.Models;
using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Extensions.Notifications;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;

namespace ExpressAgent.Platform.Services
{
    public class VMService : PlatformServiceBase<VoicemailApi>, INotifyPropertyChanged
    {
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();
        public VMService(VoicemailApi apiInstance, AgentSession session) : base(apiInstance, session)
        {
            VoicemailMessages = new ObservableCollection<ExpressVoicemailMessage>();
        }

        public ObservableCollection<ExpressVoicemailMessage> VoicemailMessages { get; set; }


        int _UnReadVoiceMailCount = 0;
        public int UnReadVoiceMailCount 
        { 
            get { return _UnReadVoiceMailCount; } 
            set 
            { 
                if (_UnReadVoiceMailCount != value) { _UnReadVoiceMailCount = value; OnPropertyChanged("UnReadVoiceMailCount"); }
            } 
        }

        public void UpdateUnReadVoiceMailCount() 
        {
            int vmnum = VoicemailMessages.Where(x => x.Read == false).ToList().Count;
            UnReadVoiceMailCount = vmnum;
        }


        async public void SetVoiceMails() 
        {
            List<VoicemailMessage> vms = null;
            await Task.Run(() =>
            {
                vms = GetVoiceMails();
            });

            VoicemailMessages.Clear();
            foreach (var msg in vms.Where(x=>x.Deleted == false))
            {
                var expVM = new ExpressVoicemailMessage(msg.Id);
                FromVoicemailMessage(ref expVM, msg);
                VoicemailMessages.Add(expVM);
            }
            UpdateUnReadVoiceMailCount();
        }

        void FromVoicemailMessage(ref ExpressVoicemailMessage vm, VoicemailMessage msg)
        {
            vm.Read = msg.Read;
            vm.AudioRecordingDurationSeconds = msg.AudioRecordingDurationSeconds;
            vm.CreatedDate = msg.CreatedDate;
            vm.CallerUser = msg.CallerUser;
            vm.Queue = msg.Queue;
            vm.Conversation = msg.Conversation;
        }

        void FromVoicemailMessageNotification(ref ExpressVoicemailMessage vm, VoicemailMessagesTopicVoicemailMessage msg)
        {
            vm.Read = msg.Read;
            vm.AudioRecordingDurationSeconds = msg.AudioRecordingDurationSeconds;
            vm.CreatedDate = msg.CreatedDate;

            //vm.Conversation = msg.;
            //vm.CallerUser = msg.CallerUser;
            //vm.Queue = msg.Queue;
        }//*/
        

        public List<VoicemailMessage> GetVoiceMails() 
        {
            //VoicemailMailboxInfo info = ApiInstance.GetVoicemailMeMailbox();
            //info.TotalCount + info.DeletedCount + info.UnreadCount + info.UsageSizeBytes
            //VoicemailUserPolicy policy = ApiInstance.GetVoicemailMePolicy();
            VoicemailMessageEntityListing msgs = ApiInstance.GetVoicemailMeMessages();
            return msgs.Entities;
        }


        public VoicemailMediaInfo GetVoiceMailMedia(string Id) 
        {
            VoicemailMediaInfo info = ApiInstance.GetVoicemailMessageMedia(Id);
            return info;
        }

        public void MarkVoiceMailRead(string Id, bool bRead = false)
        {
            ApiInstance.PatchVoicemailMessage(Id,body: new VoicemailMessage() { Read = bRead });
        }

        public void DeleteVoiceMail(string Id)
        {
            ApiInstance.PatchVoicemailMessage(Id, body: new VoicemailMessage() { Deleted = true });
        }

        
        #region Notification handlers
        public void HandleVoicemailMessageEvent(NotificationData<VoicemailMessagesTopicVoicemailMessage> voicemailMessageEvent)
        {
            Log.Debug($"VMService: voicemailmessage event received: {voicemailMessageEvent.EventBody}");

            //CurrentRoutingState = FromUserRoutingStatus(routingStatusEvent.EventBody.RoutingStatus);

            ExpressVoicemailMessage expVM = VoicemailMessages.Where(c => c.Id == voicemailMessageEvent.EventBody.Id).FirstOrDefault();
            if (voicemailMessageEvent.EventBody.Deleted == true)
            {
                //delete
                if (expVM != null)
                {
                    VoicemailMessages.Remove(expVM);
                    UpdateUnReadVoiceMailCount();
                }
            }
            else 
            {
                bool isExisting = false;
                if (expVM != null)
                {
                    isExisting = true;
                }
                else
                {
                    expVM = new ExpressVoicemailMessage(voicemailMessageEvent.EventBody.Id); //just add the id
                }
                FromVoicemailMessageNotification(ref expVM, voicemailMessageEvent.EventBody);
                if (!isExisting)
                {
                    VoicemailMessages.Add(expVM);
                    UpdateUnReadVoiceMailCount();
                }
            }
        }

        #endregion
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}