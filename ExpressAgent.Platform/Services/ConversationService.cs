﻿using ExpressAgent.Platform.Abstracts;
using ExpressAgent.Platform.Enums;
using ExpressAgent.Platform.Models;
using Microsoft.VisualBasic.ApplicationServices;
using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Client;
using PureCloudPlatform.Client.V2.Extensions.Notifications;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Documents;
using WebSocketSharp;
//using static PureCloudPlatform.Client.V2.Model.ConsultTransferToAgent;
using static PureCloudPlatform.Client.V2.Model.ConsultTransfer;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Tab;

namespace ExpressAgent.Platform.Services
{
    public class ConversationService : PlatformServiceBase<ConversationsApi>
    {
        public event EventHandler<AlertingEventArgs> Alerting;
        public event EventHandler<NotificationData<ConversationEventTopicConversation>> OnConversationEvent;

        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();
        public ConversationService(ConversationsApi apiInstance, AgentSession session) : base(apiInstance, session)
        {
            ActiveConversations = new ObservableCollection<ExpressConversation>();
            CallHistoryConversations = new ObservableCollection<CallHistoryConversation>();
        }

        #region ActiveConversations
        public ObservableCollection<ExpressConversation> ActiveConversations { get; set; }

        private ExpressConversation _ActiveConversation = null;
        public ExpressConversation ActiveConversation
        {
            get
            {
                return _ActiveConversation;
            }
            set
            {
                if (value != _ActiveConversation)
                {
                    _ActiveConversation = value;
                    OnPropertyChanged();
                }
            }
        }
        private ObservableCollection<ExpressConversation> GetActiveConversations()
        {
            try
            {

                Log.Debug($"ConversationService: Calling GetConversations");

                ObservableCollection<ExpressConversation> conversations = new ObservableCollection<ExpressConversation>();

                //get active conversations
                ConversationEntityListing result = ApiInstance.GetConversations();

                foreach (Conversation conv in result.Entities)
                {

                    SaveToFilesystem("StartupInteractions", conv.Id, conv.ToJson());

                    ExpressConversation expConv = new ExpressConversation(this, conv.Id);

                    if (UpdateExpressConversation(ref expConv, conv))
                    {
                        conversations.Add(expConv);
                    }
                }
                ActiveConversation = conversations.FirstOrDefault();
                return conversations;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

            return new ObservableCollection<ExpressConversation>();
        }

        //called by Session_Authenticated
        public void SetActiveConversations()
        {
            ActiveConversations = GetActiveConversations();
        }
        #endregion


        #region CallControl
        public void UpdateCallRecordingState(string conversationId, bool isPaused)
        {
            try
            {
                Conversation body = new Conversation()
                {
                    RecordingState = isPaused == true ? Conversation.RecordingStateEnum.Paused : Conversation.RecordingStateEnum.Active
                    //RecordingState = Paused == true ? Conversation.RecordingStateEnum.Paused : Conversation.RecordingStateEnum.None;
                };

                Log.Debug($"ConversationService: Calling PatchConversationsCall Paused:" + isPaused);

                ApiInstance.PatchConversationsCall(conversationId, body);
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }
        }
        public void UpdateCallDTMF(string conversationId, string participantId,string digits)
        {
            try
            {

                Log.Debug($"ConversationService: Calling PostConversationParticipantDigits digits:" + digits);

                ApiInstance.PostConversationParticipantDigits(conversationId, participantId , new Digits(digits) );
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }
        }



        //State: The state to update to set for this participant's communications. Possible values are: 'connected' and 'disconnected'..
        //Held: True to hold this conversation participant..
        //Muted: True to mute this conversation participant..
        //Recording: True to enable recording of this participant, otherwise false to disable recording..

        //Wrapup: Wrap-up to assign to this participant..
        //Confined: True to confine this conversation participant. Should only be used for ad-hoc conferences.
        //WrapupSkipped: True to skip wrap-up for this participant..
        public void UpdateParticipant(string conversationId, string participantId, MediaParticipantRequest body)
        {
            try
            {
                Log.Debug($"ConversationService: Calling PatchConversationParticipant");

                ApiInstance.PatchConversationParticipant(conversationId, participantId, body);
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }
        }


        public void RequestAftercallwork(string conversationId, string participantId, string communicationId, AfterCallWorkUpdate body)
        {
            try
            {
                Log.Debug($"ConversationService: Calling PatchConversationParticipant");
                AfterCallWorkUpdate result = ApiInstance.PatchConversationsAftercallworkConversationIdParticipantCommunication(conversationId, participantId, communicationId, body);
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }

        }



        public void UpdateParticipantFlagged(string conversationId, string participantId, bool isFlagged)
        {
            try
            {
                Log.Debug($"ConversationService: Calling UpdateParticipantFlagged:" + isFlagged);
                if (isFlagged == true)
                {
                    ApiInstance.PutConversationParticipantFlaggedreason(conversationId, participantId);
                }
                else 
                {
                    ApiInstance.DeleteConversationParticipantFlaggedreason(conversationId, participantId);
                }
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }
        }
        public void CreateCall(ConversationTarget targetType, string target, string callFromQueueId = null)
        {
            try
            { 
                CreateCallRequest request = new CreateCallRequest();

                switch (targetType)
                {
                    case ConversationTarget.PhoneNumber:
                        request.PhoneNumber = target;
                        break;
                    case ConversationTarget.Queue:
                        request.CallQueueId = target;
                        break;
                    case ConversationTarget.User:
                        request.CallUserId = target;
                        break;
                }

                if (!string.IsNullOrEmpty(callFromQueueId))
                {
                    request.CallFromQueueId = callFromQueueId;
                }

                ApiInstance.PostConversationsCalls(request);
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }
        }

        public void BlindTransfer(string conversationId, string participantId,ConversationTarget targetType, string target)
        {
            try
            {

                TransferRequest request = new TransferRequest();
                switch (targetType)
                {
                    case ConversationTarget.PhoneNumber:
                        request.Address = target;
                        break;
                    case ConversationTarget.Queue:
                        request.QueueId = target;
                        break;
                    case ConversationTarget.User:
                        request.UserId = target;
                        break;
                }


                ApiInstance.PostConversationParticipantReplace(conversationId, participantId, request);

            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
            }
        }

        //create the transfer
        public ConsultTransferResponse CreateConsult(string conversationId, string participantId, ConversationTarget targetType, string target)
        {
            try
            {

                Destination destination = new Destination();
                switch (targetType)
                {
                    case ConversationTarget.PhoneNumber:
                        destination.Address = target;
                        break;
                    case ConversationTarget.Queue:
                        destination.QueueId = target;
                        break;
                    case ConversationTarget.User:
                        destination.UserId = target;
                        break;
                }
                //SpeakToEnum SpeakToDest = SpeakToEnum.Destination;
                //ConsultTransfer body = new ConsultTransfer() { ConsultingUserId = ConsultingUserId, SpeakTo = SpeakToDest, Destination = destination };
                //ConsultTransfer body = new ConsultTransfer() { SpeakTo = SpeakToDest, Destination = destination };
                ConsultTransfer body = new ConsultTransfer() { Destination = destination };


                
                ConsultTransferResponse resp = ApiInstance.PostConversationsCallParticipantConsult(conversationId, participantId, body);
                return resp;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);
                return null;
            }
        }

        public void UpdateConsult(string conversationId, string participantId, ConsultTransferUpdate.SpeakToEnum speakto = ConsultTransferUpdate.SpeakToEnum.Both)
        {
            ConsultTransferUpdate body = new ConsultTransferUpdate() { SpeakTo = speakto };
            ApiInstance.PatchConversationsCallParticipantConsult(conversationId, participantId, body);
        }


            //cancel the transfer
        public void DeleteConsult(string conversationId, string participantId) 
        {
            ApiInstance.DeleteConversationsCallParticipantConsult(conversationId, participantId);
        }

        #endregion

        #region Callback
        public CreateCallbackResponse CreateCallback(DateTime callbackScheduledTime, string callbackNumber, string queueId, string userId = "", string callbackUserName="")// PureCloudPlatform.Client.V2.Model.UserMe user) 
        {

            CreateCallbackCommand body = new CreateCallbackCommand() {
                CallbackScheduledTime = callbackScheduledTime,
                CallbackNumbers = new List<string>() { callbackNumber },
                QueueId = queueId,
                RoutingData = new RoutingData()
                {
                    QueueId = queueId,
                    PreferredAgentIds = new List<string>() { userId },
                    ScoredAgents = new List<ScoredAgent>()
                },
                CountryCode = "US",
                CallbackUserName = callbackUserName
            };

            /*
            //create agent owned callback
            if (user!=null) 
            {
                var agent = new DomainEntityRef(user.Id, user.Name,user.SelfUri);
                routingData.ScoredAgents = new List<ScoredAgent>() { new ScoredAgent() { Agent = agent, Score = 100 } };
                routingData.RoutingFlags = new List<string>() { "AGENT_OWNED_CALLBACK" };
            }//*/

            CreateCallbackResponse response =  ApiInstance.PostConversationsCallbacks(body);
            return response;
        }

        #endregion

        public List<WrapupCode> GetConversationParticipantWrapupCodes(string conversationId, string participantId)
        {
            try
            {
                Log.Debug($"ConversationService: Calling GetConversationParticipantWrapupcodes");

                List<WrapupCode> codes = ApiInstance.GetConversationParticipantWrapupcodes(conversationId, participantId);

                return codes;
            }
            catch (ApiException e)
            {
                agentSession.HandleException(e);

                return new List<WrapupCode>();
            }
        }

        #region CallHistory
        public ObservableCollection<CallHistoryConversation> CallHistoryConversations { get; set; }

        async public void SetCallHistoryConversations()
        {
            List<CallHistoryConversation> callHistory = null;

            await Task.Run(() =>
            {
                callHistory = GetCallHistory();
            });

            CallHistoryConversations.Clear();
            foreach (var item in callHistory)
            {
                CallHistoryConversations.Add(item);
            }
            //UpdateUnReadVoiceMailCount();
        }

        public List<CallHistoryConversation> GetCallHistory()
        {
            
            List<string> expand = new List<string>() { "queue", "user", "externalcontact" };
            //var result = ApiInstance.GetAnalyticsConversationsDetails();
            CallHistoryConversationEntityListing result = ApiInstance.GetConversationsCallsHistory(50, 0, null, expand);
            return result.Entities.ToList();
        }
        #endregion

        /*
                  "state": "alerting",
          "initialState": "alerting",
          "recordingState": "none",
          "direction": "inbound",
          "id": "14f2d8f7-2675-4df6-ac6b-18c854c73c66",
          "recording": false,
          "muted": false,
          "confined": false,
          "held": false,
          "securePause": false,
          "self": {
            "nameRaw": "Zsombor Heffter",
        //*/
        #region Conversion
        public bool UpdateExpressConversation(ref ExpressConversation expConv, dynamic conv)
        {
            // conv must be of type ConversationSrv or ConversationEventTopicConversation or ConversationEventTopicCall
            // the dynamic is less than ideal but ConversationSrv and ConversationEventTopicConversation don't inherit from a common class


            bool isActiveConversation = false;

            expConv.Id = conv.Id;

            foreach (var participant in conv.Participants)
            {
                #region add party or find existing one
                bool isNewParty = false;
                ExpressConversationParticipant expParty = expConv.Participants.Where(p => p.Id == participant.Id).FirstOrDefault();
                if (expParty == null)
                {
                    expParty = new ExpressConversationParticipant(expConv) { Id = participant.Id };
                    isNewParty = true;
                }
                #endregion

                expParty.WrapupRequired = participant.WrapupRequired;
                expParty.WrapupTimeoutMs = participant.WrapupTimeoutMs;
                expParty.StartAcwTime = participant.StartAcwTime;
                expParty.Purpose = participant.Purpose;
                expParty.FlaggedReason = participant.FlaggedReason;
                expParty.Address = participant.Address;
                expParty.Name = participant.Name;
                expParty.UserId = participant.UserId;
                expParty.ConsultParticipantId = participant.ConsultParticipantId;

                //calc props
                expParty.Queue = agentSession.RoutingSrv.Queues.Where(q => q.Id == participant.QueueId).FirstOrDefault();
                expParty.User = agentSession.UsersSrv.Users.Where(q => q.Id == participant.UserId).FirstOrDefault();
                switch (expParty.Purpose) 
                {
                    case "user":
                        expParty.DisplayName = expParty.User.Name;
                        break;
                    case "queue":
                        expParty.DisplayName = expParty.Queue.Name;
                        break;
                    default:
                        expParty.DisplayName = expParty.Address;
                        break;
                }
                

                if (participant.Calls != null)
                {
                    foreach (var call in participant.Calls)
                    {
                        #region add call or find existing one

                        bool isNewCall = false;
                        ExpressConversationParticipantCall expCall = (ExpressConversationParticipantCall)expParty.Communications.Where(c => c is ExpressConversationParticipantCall && c.Id == call.Id).FirstOrDefault();

                        if (expCall == null)
                        {
                            expCall = new ExpressConversationParticipantCall(expParty) { Id = call.Id };
                            isNewCall = true;
                        }
                        #endregion

                        string newCallState = call.State.ToString();
                        if (expParty.UserId == agentSession.CurrentUser.Id && newCallState == "Alerting" && expCall.State != newCallState && call.Direction.ToString() == "Inbound")
                        {
                            Alerting?.Invoke(this, new AlertingEventArgs() { IsAlerting = true, Name = call.Other.Name , ConversationId = conv.Id, CallId = call.Id });
                        }
                        if (expParty.UserId == agentSession.CurrentUser.Id && expCall.State == "Alerting" && expCall.State != newCallState && call.Direction.ToString() == "Inbound")
                        {
                            Alerting?.Invoke(this, new AlertingEventArgs() { IsAlerting = false, Name = call.Other.Name, ConversationId = conv.Id, CallId = call.Id });
                        }

                        expCall.Direction = call.Direction.ToString();
                        expCall.ConnectedTime = call.ConnectedTime;
                        expCall.State = call.State.ToString();
                        expCall.Held = call.Held;
                        expCall.Muted = call.Muted;
                        expCall.Recording = call.Recording;
                        expCall.Confined = call.Confined;
                        expCall.Flagged = string.IsNullOrEmpty(expParty.FlaggedReason) ? false : true;
                        expCall.RecordingState = call.RecordingState?.ToString();
                        expCall.AfterCallWorkRequired = call.AfterCallWorkRequired;

                        //ConversationEventTopicCall tcall = call as ConversationEventTopicCall;
                        //tcall.Direction
                        //tcall.Confined


                        if (isNewCall)
                        {
                            expParty.Communications.Add(expCall);
                        }

                        // conversation is active if the communication is not disconnected/terminated, or if awaiting wrapup
                        if (expParty.UserId == agentSession.CurrentUser.Id
                            && ((expCall.State != "Disconnected" && expCall.State != "Terminated") || (expParty.WrapupRequired == true && (participant.Wrapup == null || string.IsNullOrEmpty(participant.Wrapup.Code)))))
                        {
                            isActiveConversation = true;
                        }
                    }
                }

                if (isNewParty)
                {
                    expConv.Participants.Add(expParty);

                    // send PropertyChanged notification if this participant became one of the convenience properties
                    if (expConv.AgentParticipant?.Id == expParty.Id)
                    {
                        expConv.OnPropertyChanged("AgentParticipant");
                    }

                    if (expConv.RemoteParticipant?.Id == expParty.Id)
                    {
                        expConv.OnPropertyChanged("RemoteParticipant");
                    }
                }
            }

            return isActiveConversation;
        }
        #endregion

        #region Notification handlers
        public void HandleConversationEvent(NotificationData<ConversationEventTopicConversation> conversationEvent)
        {

            //PureCloudPlatform.Client.V2.Model.ConversationEventTopicParticipant part = conversationEvent.EventBody.Participants.FirstOrDefault();
            Log.Debug($"ConversationService: Conversation event received for conversation " + conversationEvent.EventBody.ToJson());

            if(OnConversationEvent!=null) OnConversationEvent.Invoke(this,conversationEvent);

            bool isExisting = false;
            ExpressConversation expConv = ActiveConversations.Where(c => c.Id == conversationEvent.EventBody.Id).FirstOrDefault();
            if (expConv != null)
            {
                isExisting = true;
            }
            else
            {
                expConv = new ExpressConversation(this, conversationEvent.EventBody.Id); //just add the id
            }

            bool isActive = UpdateExpressConversation(ref expConv, conversationEvent.EventBody);
            if (isActive == true)
            {
                // active conversation, add it to collection if it isn't already
                if (!isExisting)
                {
                    ActiveConversations.Add(expConv);
                }
                //set it as ActiveConversation, if no other is active
                if (ActiveConversation == null)
                {
                    ActiveConversation = expConv;
                }
            }
            else
            {
                //if current is active then set the first other to active
                if (ActiveConversation != null && ActiveConversation.Id == expConv.Id)
                {
                    ActiveConversation = ActiveConversations.FirstOrDefault();
                }

                // not an active conversation, remove it from collection if it exists
                if (isExisting)
                {
                    ActiveConversations.Remove(expConv);
                }

            }

        }
        #endregion
    }

    public class AlertingEventArgs : EventArgs
    {
        public bool IsAlerting { get; set; }
        public string Name { get; set; }
        public string ConversationId { get; set; }
        public string CallId { get; set; }
        
    }
}
