﻿using ExpressAgent.Platform.Abstracts;
using PureCloudPlatform.Client.V2.Model;
using System.Diagnostics;

namespace ExpressAgent.Platform.Models
{
    public class ExpressConversationParticipantCall : ExpressConversationParticipantCommunication
    {
        public ExpressConversationParticipantCall(ExpressConversationParticipant participant) : base(participant)
        { }


        private bool? _Muted;
        public bool? Muted
        {
            get
            {
                return _Muted;
            }
            set
            {
                if (value != _Muted)
                {
                    _Muted = value;
                    OnPropertyChanged();
                }
            }
        }


        public void ToggleMute()
        {
            if (Participant == null)
            {
                base.Log.Debug($"ExpressConversationParticipantCall: Unable to toggle mute state due to missing participant on {Id}");
                return;
            }

            MediaParticipantRequest body = new MediaParticipantRequest()
            {
                Muted = !Muted ?? true
            };

            Participant.Conversation.ConversationService.UpdateParticipant(Participant.Conversation.Id, Participant.Id, body);
        }
    }
}
