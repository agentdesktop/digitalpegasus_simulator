﻿using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ExpressAgent.Platform.Models
{
    public class ExpressVoicemailMessage : INotifyPropertyChanged
    {
        public ExpressVoicemailMessage(string Id) { this.Id = Id; }

        public string Id { get; private set; }
        public Conversation Conversation { get; set; }
        public string Name { get; set; }
        bool? _Read;
        public bool? Read { get { return _Read; } set { if (_Read != value) { _Read = value; OnPropertyChanged(); } } }
        public int? AudioRecordingDurationSeconds { get; set; }
        public DateTime? CreatedDate { get; set; }
        public User CallerUser { get; set; }
        public Queue Queue { get; set; }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        internal virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
