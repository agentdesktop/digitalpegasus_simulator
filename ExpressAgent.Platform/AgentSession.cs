﻿using ExpressAgent.Auth;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using PureCloudPlatform.Client.V2.Client;
using PureCloudPlatform.Client.V2.Model;
using System.Diagnostics;
using System.Linq;
using ExpressAgent.Notifications;
using ExpressAgent.Platform.Models;
using ExpressAgent.Platform.Services;
using PureCloudPlatform.Client.V2.Api;

namespace ExpressAgent.Platform
{
    public class AgentSession : INotifyPropertyChanged, IDisposable
    {
        #region BindedServices
        public AnaliticsService AnaliticsSrv { get; set; }
        public ConversationService ConversationSrv { get; set; }
        public PresenceService PresenceSrv { get; set; }
        public RoutingService RoutingSrv { get; set; }
        public UserService UsersSrv { get; set; }
        public VMService VMSrv { get; set; }
        public UserRecordingsService URSrv { get; set; }
        public SettingsService SettingsSrv { get; set; }
        
        #endregion

        public Languages Lang { get; set; }

        #region Binded props
        private UserMe _CurrentUser;
        public UserMe CurrentUser
        {
            get
            {
                return _CurrentUser;
            }
            set
            {
                if (value != _CurrentUser)
                {
                    _CurrentUser = value;
                    OnPropertyChanged();
                }
            }
        }

        private void GetCurrentUser() 
        {
            CurrentUser = UsersSrv.GetCurrentUser();
        }
        #endregion

        public Websocket NotificationsWebsocket;
        public EventHandler Authenticated;
        public EventHandler Unauthenticated;

        private bool LoggingOut;
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        public AgentSession()
        {

          
            //Configuration.Default.ApiClient.setBasePath(PureCloudRegionHosts.eu_central_1);
            //Configuration.Default.ApiClient.setBasePath(PureCloudRegionHosts.eu_central_1.GetDescription());
            Configuration.Default.ApiClient.setBasePath("https://api." + AuthSession.Current.Environment);

            Configuration.Default.ApiClient.RetryConfig = new ApiClient.RetryConfiguration
            {
                MaxRetryTimeSec = 10,
                RetryMax = 5,
                RetryAfterDefaultMs = 5,
            };

            Configuration.Default.AutoReloadConfig = false;
            Configuration.Default.ShouldRefreshAccessToken = true;

            Configuration.Default.Logger.Level = LogLevel.LTrace;
            Configuration.Default.Logger.Format = LogFormat.JSON;
            Configuration.Default.Logger.LogRequestBody = true;
            Configuration.Default.Logger.LogResponseBody = true;
            Configuration.Default.Logger.LogToConsole = true;
            Configuration.Default.Logger.LogFilePath = "dotnetsdk.log";


            AnaliticsSrv = new AnaliticsService(new AnalyticsApi(), this);
            ConversationSrv = new ConversationService(new ConversationsApi(), this);
            PresenceSrv = new PresenceService(new PresenceApi(), this);
            RoutingSrv = new RoutingService(new RoutingApi(), this);
            UsersSrv = new UserService(new UsersApi(), this);
            VMSrv = new VMService(new VoicemailApi(), this);
            URSrv = new UserRecordingsService(new UserRecordingsApi(), this);
            SettingsSrv = new SettingsService(new SettingsApi(), this);
            

            if (!AuthSession.Current.HasToken)
            {
                AuthSession.Current.Authenticate();
            }
            
            AuthSession.Current.PropertyChanged += AuthSession_PropertyChanged;

            Authenticated += Session_Authenticated;
            Unauthenticated += Session_Unauthenticated;
        }


        private void Session_Authenticated(object sender, EventArgs e)
        {
            try
            {
                //subscriibe for Conversation,Presence and RoutingStatusEvents
                NotificationsWebsocket = new Websocket(CurrentUser.Id)
                {
                    ConversationEventDelegate = ConversationSrv.HandleConversationEvent,
                    PresenceEventDelegate = PresenceSrv.HandlePresenceEvent,
                    RoutingStatusEventDelegate = UsersSrv.HandleRoutingStatusEvent,
                    VoicemailMessageEventDelegate = VMSrv.HandleVoicemailMessageEvent,
                    UsersRecordingEventDelegate = URSrv.HandleUserRecordingEvent,
                };


                PresenceSrv.SetInitialPresence();
                RoutingSrv.SetQueueCollection();
                UsersSrv.SetUserCollection();
                ConversationSrv.SetActiveConversations();
                //async
                VMSrv.SetVoiceMails();
                ConversationSrv.SetCallHistoryConversations();
                URSrv.SetUserRecordings();
            }
            catch (Exception ex)
            {
                Log.Error("Session_Authenticated error:" + ex.Message);

            }
        }


        private void AuthSession_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "AuthToken" && AuthSession.Current.HasToken)
            {
                LoggingOut = false;
                Configuration.Default.AccessToken = (sender as AuthSession).AuthToken;

                //read current user using UsersService
                GetCurrentUser();

                Log.Debug("Session: Authenticated successfully");

                Authenticated?.Invoke(this, new EventArgs());
            }
        }

        private void Session_Unauthenticated(object sender, EventArgs e)
        {
            NotificationsWebsocket?.Dispose();

            if (!LoggingOut)
            {
                AuthSession.Current.Authenticate(true);
            }
        }

        public void HandleException(ApiException e)
        {
            Log.Error("HandleException:" + e.Message);

            if (e.ErrorCode == 401)
            {
                Unauthenticated?.Invoke(this, new EventArgs());
            }
        }
        public void Logout()
        {
            LoggingOut = true;
            AuthSession.Current.Logout();
            Unauthenticated?.Invoke(this, new EventArgs());
        }

        public void Dispose()
        {
            NotificationsWebsocket?.Dispose();
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
