﻿using NLog.Fluent;
using PureCloudPlatform.Client.V2.Extensions.Notifications;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;

namespace ExpressAgent.Notifications
{
    public class Websocket : IDisposable
    {
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        private NotificationHandler Handler = new NotificationHandler();
        private List<Tuple<string, Type>> Subscriptions;

        public ConversationEventDelegate ConversationEventDelegate;
        public PresenceEventDelegate PresenceEventDelegate;
        public RoutingStatusEventDelegate RoutingStatusEventDelegate;
        public VoicemailMessageEventDelegate VoicemailMessageEventDelegate;
        public UsersRecordingEventDelegate UsersRecordingEventDelegate;


        EventLogger ConversationEventLogger = new EventLogger() { EventDir = "conversation" };
        EventLogger VoicemailMessageEventLogger = new EventLogger() { EventDir = "voicemail" };
        public Websocket(string userId)
        {
            Subscribe(userId);

            Handler.NotificationReceived += Handler_NotificationReceived;
        }

        private void Handler_NotificationReceived(INotificationData notificationData)
        {
            Application.Current.Dispatcher.BeginInvoke(new System.Action(() =>
            {
                switch (notificationData)
                {
                    case NotificationData<ConversationEventTopicConversation> conversationEvent:
                        if (ConversationEventDelegate != null)
                        {
                            ConversationEventLogger.LogEvent(conversationEvent);
                            ConversationEventDelegate(conversationEvent);
                        }
                        else
                        {
                            Log.Debug("Websocket: ConversationEventDelegate is null");
                        }
                        break;
                    case NotificationData<PresenceEventUserPresence> presenceEvent:
                        if (PresenceEventDelegate != null)
                        {
                            PresenceEventDelegate(presenceEvent);
                        }
                        else
                        {
                            Log.Debug("Websocket: PresenceEventDelegate is null");
                        }
                        break;
                    case NotificationData<VoicemailMessagesTopicVoicemailMessage> voicemailMessage:
                        if (VoicemailMessageEventDelegate != null)
                        {
                            VoicemailMessageEventLogger.LogEvent(voicemailMessage);
                            VoicemailMessageEventDelegate(voicemailMessage);
                        }
                        else
                        {
                            Log.Debug("Websocket: VoicemailMessageEventDelegate is null");
                        }
                        break;
                    case NotificationData<AdhocRecordingTopicRecordingDataV2> UsersRecording:
                        if (UsersRecordingEventDelegate != null)
                        {
                            //UsersRecordingEventLogger.LogEvent(UsersRecording);
                            UsersRecordingEventDelegate(UsersRecording);
                        }
                        else
                        {
                            Log.Debug("Websocket: UsersRecordingEventDelegate is null");
                        }
                        break;
                    case NotificationData<UserRoutingStatusUserRoutingStatus> routingStatusEvent:
                        if (RoutingStatusEventDelegate != null)
                        {
                            RoutingStatusEventDelegate(routingStatusEvent);
                        }
                        else
                        {
                            Log.Debug("Websocket: RoutingStatusEventDelegate is null");
                        }
                        break;
                    case NotificationData<ChannelMetadataNotification> metadataEvent:
                        Log.Debug("Websocket: Received metadata");
                        break;
                    default:
                        Log.Debug($"Websocket: Notification event type not recognized: {notificationData}");
                        break;
                }
            }));
        }

        private void Subscribe(string userId)
        {
            Subscriptions = new List<Tuple<string, Type>>();

            Subscriptions.Add(new Tuple<string, Type>($"v2.users.{userId}.conversations",typeof(ConversationEventTopicConversation)));
            Subscriptions.Add(new Tuple<string, Type>($"v2.users.{userId}.presence",typeof(PresenceEventUserPresence)));
            Subscriptions.Add(new Tuple<string, Type>($"v2.users.{userId}.routingStatus",typeof(UserRoutingStatusUserRoutingStatus)));
            Subscriptions.Add(new Tuple<string, Type>($"v2.users.{userId}.voicemail.messages",typeof(VoicemailMessagesTopicVoicemailMessage)));
            Subscriptions.Add(new Tuple<string, Type>($"v2.users.{userId}.userrecordings",typeof(AdhocRecordingTopicRecordingDataV2)));

            Handler.AddSubscriptions(Subscriptions);

            if(Handler.WebSocket.IsAlive)
            Log.Debug("Websocket: Connected successfully");
        }

        public void Dispose()
        {
            Handler.Dispose();

            if(!Handler.WebSocket.IsAlive)
            Log.Debug("Websocket: Disconnected successfully");
        }
    }


    public class EventLogger
    {
        #region log events to filesystem for debugging
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        Dictionary<string, int> loggedEvents = new Dictionary<string, int>();

        public string EventDir { get; set; }

        public void LogEvent(dynamic Event)
        {
            try
            {
                var currdir = Directory.GetCurrentDirectory();

                string eventsdir = currdir + "\\" + "events";
                if (!Directory.Exists(eventsdir)) Directory.CreateDirectory(eventsdir);

                eventsdir = eventsdir + "\\" + EventDir;
                if (!Directory.Exists(eventsdir)) Directory.CreateDirectory(eventsdir);


                string convdir = eventsdir + "\\" + Event.EventBody.Id;
                if (!Directory.Exists(convdir)) Directory.CreateDirectory(convdir);


                int event_number = 1;
                string event_id = Event.EventBody.Id;
                lock (loggedEvents)
                {
                    if (!loggedEvents.ContainsKey(event_id))
                    {
                        loggedEvents.Add(event_id, event_number);
                    }
                    else
                    {
                        loggedEvents[event_id]++;
                        event_number = loggedEvents[event_id];
                    }
                }

                File.WriteAllText(convdir + "\\" + event_number.ToString("00") + ".json", Event.EventBody.ToJson());

            }
            catch (Exception ex)
            {
                Log.Error("LogEvent "+ EventDir + " error:" + ex);
            }
        }
        #endregion
    }
}
