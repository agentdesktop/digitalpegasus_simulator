﻿using ExpressAgent.Controls;
using ExpressAgent.Platform;
using ExpressAgent.Platform.Models;
using ExpressAgent.Platform.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace ExpressAgent
{
    /// <summary>
    /// Interaction logic for AgentWindow.xaml
    /// </summary>
    public partial class AgentWindow : Window
    {
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();



        public ObservableCollection<WebDevice> RingDevices = new ObservableCollection<WebDevice>();
        public ObservableCollection<WebDevice> MicDevices = new ObservableCollection<WebDevice>();

        public static AgentWindow Instance { get; private set; }
        public bool QuitOnClose = true;
        public AgentSession agentSession;
        private System.Timers.Timer DurationRefreshTimer = new(interval: 1000);
        public AgentWindow(AgentSession agentSession)
        {
            Instance = this;
            InitializeComponent();
            this.agentSession = agentSession;
            var cboLangItemsSource = Languages.GetInstance().DisplayLanguages;
            this.agentSession.Lang = Languages.GetInstance();
            DataContext = agentSession;
            SetAppState(CurrentAppState);
            SetAppDialog(CurrentAppDialog);
            Topmost = !Debugger.IsAttached;
            agentSession.ConversationSrv.Alerting += ConversationSrv_Alerting;
            //agentSession.ConversationSrv.OnConversationEvent += ConversationSrv_OnConversationEvent;
            DurationRefreshTimer.Elapsed += DurationRefreshTimer_Elapsed;
            DurationRefreshTimer.Start();

        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            if (QuitOnClose)
            {
                Application.Current.Shutdown();
            }
        }

        #region Timer used to refresh status bar time
        private void DurationRefreshTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(delegate ()
            {
                ucStatusBar.Refresh();
            }));
        }
        #endregion

        #region Handle incoming call ringing event
        private void ConversationSrv_Alerting(object sender, Platform.Services.AlertingEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(delegate ()
            {
                if (e.IsAlerting == true)
                {
                    SetAppState(AppStates.Interactions);
                    LogMessage("Alerting ConversationId :" + e.ConversationId + ", CallId:" + e.CallId);
                    ucStatusBar.SetAlerting(e.ConversationId);
                }
                else 
                {
                    ucStatusBar.SetAlerting();

                }
                //MessageBox.Show(e.ConversationId + ", " + e.Name + ", IsAlerting:" + e.IsAlerting);
            }));
        }
        public void AcceptPendingSession(string conversationId)
        {
            Dispatcher.BeginInvoke(new Action(delegate ()
            {
                string micDeviceId = "default";
                ucWebRtcHost.AcceptPendingSession(conversationId,micDeviceId);
            }));
        }
        #endregion


        #region set app state - visible main component
        AppStates CurrentAppState = AppStates.Default;
        public void SetAppState(AppStates appState)
        {
            CurrentAppState = appState;
            ucInteractions.Visibility = appState == AppStates.Interactions ? Visibility.Visible : Visibility.Collapsed;
            ucNewCall.Visibility = appState == AppStates.NewCall ? Visibility.Visible : Visibility.Collapsed;
            ucUserInbox.Visibility = appState == AppStates.UserInbox ? Visibility.Visible : Visibility.Collapsed;
            ucQueueActivation.Visibility = appState == AppStates.QueueActivation? Visibility.Visible : Visibility.Collapsed;
            ucSettings.Visibility = appState == AppStates.Settings ? Visibility.Visible : Visibility.Collapsed;
            ucInteractionLog.Visibility = appState == AppStates.InteractionLog ? Visibility.Visible : Visibility.Collapsed;
        }

        AppDialogs CurrentAppDialog = AppDialogs.Default;

        public void SetAppDialog(AppDialogs appDialog)
        {
            CurrentAppDialog = appDialog;
            ucCallback.Visibility = appDialog == AppDialogs.CallBack ? Visibility.Visible : Visibility.Collapsed;
            ucDTMF.Visibility = appDialog == AppDialogs.DTMF ? Visibility.Visible : Visibility.Collapsed;
            ucTransfer.Visibility = appDialog == AppDialogs.Transfer ? Visibility.Visible : Visibility.Collapsed;
            ucWrapUp.Visibility = appDialog == AppDialogs.WrapUp ? Visibility.Visible : Visibility.Collapsed;
            ucRecordings.Visibility = appDialog == AppDialogs.Recordings ? Visibility.Visible : Visibility.Collapsed;
        }
        #endregion

        #region Log webrtc messages
        public void LogMessage(string txt) 
        {
            Dispatcher.BeginInvoke(new Action(delegate ()
            {
                txtLog.Text += txt + "\r\n";
            }));
        }

        #endregion

        

    }

    public enum AppStates : int
    {
        Default,
        NewCall,
        Interactions,
        UserInbox,
        QueueActivation,
        Settings,
        InteractionLog,
    }

    public enum AppDialogs : int
    {
        Default,
        CallBack,
        DTMF,
        Transfer,
        WrapUp,
        Recordings,
    }

}
