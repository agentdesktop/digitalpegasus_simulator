﻿#define USEWINMM
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Media;
using System.Windows.Resources;
#if !USEWINMM
using dotBASS;
#endif

namespace ExpressAgent
{
    public class ExSoundPlayer
    {
        public static void Play(string sSoundName)
        {
            Uri uri = new Uri("Sounds/" + sSoundName + ".wav", UriKind.Relative);
            StreamResourceInfo info = Application.GetResourceStream(uri);
            using (var soundPlayer = new SoundPlayer(info.Stream))
            {
                soundPlayer.Play(); // can also use 
                //soundPlayer.PlaySync();
            }
        }



#if USEWINMM
        #region Playsound
        private const int SND_FILENAME = 0x20000;
        private const int SND_LOOP = 8;
        private const int SND_ASYNC = 1;
        private const int SND_NODEFAULT = 2;
        private static string sRingPath = "";
        private static string sRingBackPath = "";

        [DllImport("WinMM.dll")]
        public static extern bool PlaySound(string fname, int Mod, int flag);

        public static bool Init(int RngDev, int RngVol, string sRingWavPath, string sRingBackWavPath)
        {
            if (sRingWavPath != "") sRingPath = sRingWavPath;
            if (sRingBackPath != "") sRingBackPath = sRingBackWavPath;
            return true;
        }

        public static void PlayRing(bool bRingBack)
        {
            PlaySound(bRingBack == true ? sRingBackPath : sRingPath, 0, SND_ASYNC | SND_FILENAME | SND_LOOP | SND_NODEFAULT);
        }

        public static void StopRing()
        {
            PlaySound(null, 0, SND_ASYNC | SND_FILENAME | SND_LOOP | SND_NODEFAULT);
        }

        public static Dictionary<uint, string> GetRingDevices()
        {
            return new Dictionary<uint, string>();
        }

        public static void ReInit(int RngDev, int RngVol)
        {
        }
        public static void Deinit()
        {
        }

        #endregion
#else
        #region Bass
        private static UInt32 ringStream = 0;
        private static UInt32 ringBackStream = 0;
        private static bool bBassInited = false;
        public static void Deinit()
        {
            try
            {
                if (bBassInited == true)
                {
                    // free the stream 
                    BASS.BASS_StreamFree(ringStream);
                    BASS.BASS_StreamFree(ringBackStream);
                    // free BASS 
                    BASS.BASS_Free();
                }
            }
            catch (Exception)
            {
            }
        }
        public static Dictionary<uint, string> GetRingDevices()
        {
            return BASS.BASS_GetDeviceNames();
        }

        public static void ReInit(int iDevice, int iVol)
        {
            try
            {
                if (ActualVolume != iVol && ActualDevice == iDevice)
                {
                    //set just volume
                    SetRingVolume(iVol);
                }
                else if (ActualDevice != iDevice)
                {
                    StopRing();
                    //set device and volume
                    Init(iDevice, iVol);
                }
            }catch(Exception ex)
            {
            }
        }


        private static int ActualVolume = 0;
        private static int ActualDevice = 0;
        private static string sRingWavPath = "";
        private static string sRingBackWavPath = "";
        public static bool Init(int iDevice, int iVol, string ringWavPath = "", string ringBackWavPath = "")
        {
            try
            {
                Deinit();

                ActualDevice = iDevice;
                ActualVolume = iVol;
                if (ringWavPath != "") sRingWavPath = ringWavPath;
                if (ringBackWavPath != "") sRingBackWavPath = ringBackWavPath;

                // init BASS using the default output device 
                if (BASS.BASS_Init(iDevice, 44100, BASSInitFlags.BASS_DEVICE_DEFAULTS, IntPtr.Zero))
                {

                    // create a stream channel from a file 
                    ringStream = BASS.BASS_StreamCreateFile(sRingWavPath, 0, 0, BASSFlag.BASS_DEFAULT | BASSFlag.BASS_SAMPLE_LOOP);
                    ringBackStream = BASS.BASS_StreamCreateFile(sRingBackWavPath, 0, 0, BASSFlag.BASS_DEFAULT | BASSFlag.BASS_SAMPLE_LOOP);
                    SetRingVolume(iVol);
                }
                bBassInited = true;
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }

        public static void PlayRing(bool bRingBack)
        {
            try
            {
                BASS.BASS_ChannelPlay(bRingBack == true ? ringBackStream : ringStream, true);
            }catch(Exception)
            {
            }
        }
        public static void StopRing()
        {
            try
            {
                BASS.BASS_ChannelStop(ringStream);
                BASS.BASS_ChannelStop(ringBackStream);
            }
            catch (Exception)
            {
            }
        }

        public static void SetRingVolume(int iValue)
        {
            try
            {
                float vlevel = (float)iValue / 100.0f;
                BASS.BASS_SetVolume(vlevel);
            }
            catch(Exception)
            {
            }
    }
        public static int GetRingVolume()
        {
            try
            {
                return (int)(BASS.BASS_GetVolume() * 100.0f);
            }
            catch(Exception)
            {
            }
            return 100;
        }

        #endregion

//                        

#endif


    }


}