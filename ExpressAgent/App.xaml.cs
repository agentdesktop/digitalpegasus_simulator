﻿using ExpressAgent.Auth;
using ExpressAgent.Platform;
using NLog;
using System;
using System.Dynamic;
using System.Windows;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;


namespace ExpressAgent
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private AgentWindow agentWindow;
        private AgentSession actSession;
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();
        public IDisposable webApp { get; set; }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            try
            {
                Log.Debug("Application_Startup");

                //var jsonString = "{\"foo\": \"bar\"}";
                //dynamic data = System.Text.Json.JsonSerializer.Deserialize<ExpandoObject>(jsonString);
                //string x2 = data.foo;



                this.Dispatcher.UnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(Dispatcher_UnhandledException);
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
#if !DEBUG
            int sessionID = Process.GetCurrentProcess().SessionId;
            string processname = Process.GetCurrentProcess().ProcessName;
            int processid = Process.GetCurrentProcess().Id;
            var prc = Process.GetProcesses().Where(x => x.ProcessName.Equals(processname) && x.Id != processid).FirstOrDefault();
            if (prc != null) 
            {
                if (prc.SessionId == sessionID)
                {
                    MessageBox.Show("App error: another process (processname:" + processname + ", processid:" + prc.Id + ", sessionid:" + prc.SessionId + ", machinename:" + prc.MachineName + " ) is already running, please close another instance first!");
                    this.Shutdown();
                }
            }
#endif

                //agentWindow = new AgentWindow(actSession);agentWindow.Show();

                actSession = new AgentSession();
                actSession.Authenticated += Session_Authenticated;
                actSession.Unauthenticated += Session_Unauthenticated;
            }
            catch (Exception ex)
            {
                Log.Debug("Session_Authenticated error:" + ex);

            }
        }

        #region Session events
        private void Session_Authenticated(object sender, EventArgs e)
        {
            try
            {
                Log.Debug("Session_Authenticated");

                if (agentWindow != null && agentWindow.IsVisible)
                {
                    agentWindow.QuitOnClose = false;
                    agentWindow.Close();
                }
                
                agentWindow = new AgentWindow(actSession);
                agentWindow.Show();
            }
            catch (Exception ex) 
            {
                Log.Debug("Session_Authenticated error:" + ex);

            }
        }

        private void Session_Unauthenticated(object sender, EventArgs e)
        {
            if (agentWindow != null && agentWindow.IsVisible)
            {
                Log.Debug("Session_Unauthenticated");

                agentWindow.QuitOnClose = false;
                agentWindow.Close();
            }
        }
        #endregion

        private void Application_SessionEnding(object sender, SessionEndingCancelEventArgs e)
        {
            OnExit();
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            OnExit();
        }

        private void OnExit()
        {
            Log.Debug("OnExit");

            actSession?.Dispose();
            webApp?.Dispose();
            agentWindow?.ucWebRtcHost.Destory();
        }


        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            //MessageBox.Show(ex.Message, "Uncaught Thread Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            Log.Error("CurrentDomain_UnhandledException:" + ex.ToString(), ex);
        }

        void Dispatcher_UnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs ex)
        {
            Log.Error("Dispatcher_UnhandledException:" + ex.Exception.ToString());
            ex.Handled = true;
        }

    }
}
