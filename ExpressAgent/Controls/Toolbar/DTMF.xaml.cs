﻿using ExpressAgent.Platform;
using ExpressAgent.Platform.Models;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Toolbar
{
    /// <summary>
    /// Interaction logic for DTMF.xaml
    /// </summary>
    public partial class DTMF : UserControl
    {
        private AgentSession agentSession
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }
        public DTMF()
        {
            InitializeComponent();
        }

        private void DialPad_Click(object sender, RoutedEventArgs e)
        {
            string dtmf = (sender as Button).Content.ToString().ToLower();

            ExpressConversation activeConversation = agentSession.ConversationSrv.ActiveConversation;
            if (activeConversation != null) 
            {

                string sound = dtmf;
                switch (dtmf) {
                    case "#":
                        sound = "pound";
                        break;
                    case "*":
                        sound = "star";
                        break;
                }
                ExSoundPlayer.Play("Dtmf-" + sound);

                agentSession.ConversationSrv.UpdateCallDTMF(activeConversation.Id, activeConversation.AgentParticipant.Id, dtmf);
            }
            

        }

        void Close()
        {
            AgentWindow.Instance.SetAppDialog(AppDialogs.Default);
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
