﻿using ExpressAgent.Controls;
using ExpressAgent.Platform;
using Microsoft.Extensions.Primitives;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Tab;

namespace ExpressAgent.Toolbar
{
    /// <summary>
    /// Interaction logic for Transfer.xaml
    /// </summary>
    public partial class Transfer : UserControl
    {
        public Transfer()
        {
            InitializeComponent();
            this.IsVisibleChanged += UserControl_IsVisibleChanged;
        }


        private AgentSession agentSession
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }

        void Close()
        {
            AgentWindow.Instance.SetAppDialog(AppDialogs.Default);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }




        private void bntBlind_Click(object sender, RoutedEventArgs e)
        {
            var activeConversation = agentSession.ConversationSrv.ActiveConversation;
            if (activeConversation != null && ucSearchTarget.SelectedItem!=null)
            {

                SearchTarget searchTarget = (SearchTarget)ucSearchTarget.SelectedItem;

                if (searchTarget.Type == TargetTypes.Queue)
                {
                    //transfer call to queue
                    Queue queue = searchTarget.Queue;
                    agentSession?.ConversationSrv.BlindTransfer(activeConversation.Id, activeConversation.AgentParticipant.Id, Platform.Enums.ConversationTarget.Queue, queue.Id);
                }
                else if (searchTarget.Type == TargetTypes.User)
                {
                    //transfer call to agent
                    User agent = searchTarget.User;
                    agentSession?.ConversationSrv.BlindTransfer(activeConversation.Id, activeConversation.AgentParticipant.Id, Platform.Enums.ConversationTarget.User, agent.Id);
                }
                else
                {
                    //number
                    agentSession?.ConversationSrv.BlindTransfer(activeConversation.Id, activeConversation.AgentParticipant.Id, Platform.Enums.ConversationTarget.PhoneNumber, txtTarget.Text);
                }
                Close();
            }

        }

        private void bntConsult_Click(object sender, RoutedEventArgs e)
        {
            var activeConversation = agentSession.ConversationSrv.ActiveConversation;
            if (activeConversation != null && ucSearchTarget.SelectedItem != null)
            {
                SearchTarget searchTarget = (SearchTarget)ucSearchTarget.SelectedItem;
                if (searchTarget.Type == TargetTypes.User)
                {
                    User targetUser = searchTarget.User;
                    //consult call to other agent
                    var response = agentSession?.ConversationSrv.CreateConsult(activeConversation.Id, activeConversation.RemoteParticipant.Id, Platform.Enums.ConversationTarget.User, targetUser.Id);
                    string DestinationParticipantId = response.DestinationParticipantId;
                }
                else if (searchTarget.Type == TargetTypes.Queue)
                {
                    
                    Queue targetQueue = searchTarget.Queue;
                    //consult call to other queue
                    var response = agentSession?.ConversationSrv.CreateConsult(activeConversation.Id, activeConversation.RemoteParticipant.Id, Platform.Enums.ConversationTarget.Queue, targetQueue.Id);
                    string DestinationParticipantId = response.DestinationParticipantId;
                }
                Close();
            }

        }

        async private void txtTarget_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtTarget.Text))
            {

                bool bCheckUsers = btnUser.IsChecked == true;
                bool bCheckQueues = btnQueue.IsChecked == true;
                string searchFor = txtTarget.Text;


                if (string.IsNullOrEmpty(searchFor)) 
                {
                    ucSearchTarget.ItemsSource = null;

                }
                else if (!bCheckUsers && !bCheckQueues)
                {
                    ucSearchTarget.ItemsSource = null;

                }
                else
                {

                    List<SearchTarget> Targets = new List<SearchTarget>();
                    await Task.Run(() =>
                    {
                        if (bCheckUsers == true)
                        {
                            List<User> users = agentSession?.UsersSrv.SearchTargets(searchFor);
                            foreach (var user in users)
                            {
                                Targets.Add(new SearchTarget()
                                {
                                    Name = user.Name,
                                    User = user,
                                    Type = TargetTypes.User
                                });
                            }
                        }
                        if (bCheckQueues == true)
                        {
                            List<Queue> queues = agentSession?.RoutingSrv.SearchTargets(searchFor);
                            foreach (var queue in queues)
                            {
                                Targets.Add(new SearchTarget()
                                {
                                    Name = queue.Name,
                                    Queue = queue,
                                    Type = TargetTypes.Queue
                                });

                            }
                        }
                    });
                    ucSearchTarget.ItemsSource = Targets;
                }
            }
            else
            {
                ucSearchTarget.ItemsSource = null;
            }
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                this.DataContext = AgentWindow.Instance.agentSession;
                var activeConversation = agentSession.ConversationSrv.ActiveConversation;
                if (activeConversation != null && !string.IsNullOrEmpty(activeConversation.AgentParticipant.ConsultParticipantId))
                {
                    //complete consult
                    grnInitConsult.Visibility = Visibility.Collapsed;
                    grnCompleteConsult.Visibility = Visibility.Visible;
                    string transferTest = string.Format("Are you sure you want to transfer '{0}' to '{1}'?", activeConversation.RemoteParticipant?.DisplayName, activeConversation.RemoteParticipant2?.DisplayName);
                    tbCompleteConsult.Text = transferTest;
                }
                else
                {
                    grnInitConsult.Visibility = Visibility.Visible;
                    grnCompleteConsult.Visibility = Visibility.Collapsed;
                }
                txtTarget.Clear();
                txtTarget.Focus();

                //btnUser.IsChecked = true;
                //btnQueue.IsChecked = true;
            }
            else 
            {
                ucSearchTarget.lstMain.ItemsSource = null;
            }
        }

        private void bntComplete_Click(object sender, RoutedEventArgs e)
        {
            var activeConversation = agentSession.ConversationSrv.ActiveConversation;
            if (activeConversation != null && activeConversation.AgentParticipant!=null)
            {

                var agentParticipant = activeConversation.AgentParticipant;
                MediaParticipantRequest body = new MediaParticipantRequest()
                {
                    State = MediaParticipantRequest.StateEnum.Disconnected
                };
                agentParticipant.Conversation.ConversationService.UpdateParticipant(agentParticipant.Conversation.Id, agentParticipant.Id, body);
            }
            Close();
        }

        private void bntCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
