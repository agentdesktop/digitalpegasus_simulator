﻿using ExpressAgent.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for InteractionLog.xaml
    /// </summary>
    public partial class InteractionLog : UserControl
    {
        public InteractionLog()
        {
            InitializeComponent();
        }

        void Close() 
        {
            AgentWindow.Instance.SetAppState(AppStates.Interactions);
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private AgentSession Session
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }
    }
}
