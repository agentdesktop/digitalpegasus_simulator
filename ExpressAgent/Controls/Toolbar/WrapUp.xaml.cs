﻿using ExpressAgent.Controls;
using ExpressAgent.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Toolbar
{
    /// <summary>
    /// Interaction logic for WrapUp.xaml
    /// </summary>
    public partial class WrapUp : UserControl
    {
        private AgentSession agentSession
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }
        public WrapUp()
        {
            InitializeComponent();

            
        }

        void Close() 
        {
            AgentWindow.Instance.SetAppDialog(AppDialogs.Default);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            var activeConversation = agentSession.ConversationSrv.ActiveConversation;
            if (activeConversation != null && cboWraup.SelectedItem!=null)
            {
                string WrapUpNotes = "";
                activeConversation.AgentParticipant.SetWrapUp((string)cboWraup.SelectedValue, WrapUpNotes);
                Close();
            }

        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            
            if ((bool)e.NewValue == true)
            {
                var ActiveConversation = agentSession.ConversationSrv.ActiveConversation;
                if (ActiveConversation != null)
                {
                    var codes = ActiveConversation.AgentParticipant.GetWrapUpCodes();
                    cboWraup.ItemsSource = codes;
                }
                else
                {
                    cboWraup.ItemsSource = null;
                }
            }
            
        }
    }
}
