﻿using ExpressAgent.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Toolbar
{
    /// <summary>
    /// Interaction logic for Callback.xaml
    /// </summary>
    public partial class Callback : UserControl
    {
        public Callback()
        {
            InitializeComponent();
            this.Loaded += Callback_Loaded;
        }
        private AgentSession agentSession
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }
        private void Callback_Loaded(object sender, RoutedEventArgs e)
        {
            datePicker.SelectedDate= DateTime.Now;
            timePicker.Value = DateTime.Now.AddMinutes(30);
            //The phone number defaults to the caller’s ANI.
            //By default, scheduled callbacks route to the queue that received the original interaction.
            //Note: You cannot schedule multiple callbacks for a single interaction. After you schedule a callback, the Schedule button becomes unavailable.

        }
        DateTime GetSelectedCallbackDate()
        {
            DateTime SelectedDate = datePicker.SelectedDate == null ? DateTime.Now : (DateTime)datePicker.SelectedDate;
            int Day = SelectedDate.Day;
            int Month = SelectedDate.Month;
            int Year = SelectedDate.Year;
            int Hour = timePicker.Value.Value.Hour;
            int Min = timePicker.Value.Value.Minute;
            /*if (chkTime.IsChecked == false)
            {
                Hour = 0;
                Min = 0;
            }//*/
            if (Hour == 0) Min = 0;
            return new DateTime(Year, Month, Day, Hour, Min, 0); ;
        }


        private void Close() 
        {
            AgentWindow.Instance.SetAppDialog(AppDialogs.Default);

        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            

        }

        private void btnSchedule_Click(object sender, RoutedEventArgs e)
        {

            //https://developer.genesys.cloud/routing/conversations/creating-scheduled-callbacks
            if (!string.IsNullOrEmpty(txtPhoneNumber.Text)) 
            {
                
                string queueid = agentSession.ConversationSrv.ActiveConversation.RemoteParticipant.Queue.Id;
                string callbackUserName = agentSession.ConversationSrv.ActiveConversation.RemoteParticipant.Name;
                //PureCloudPlatform.Client.V2.Model.UserMe user = chkRouteToMe.IsChecked == true ? agentSession.CurrentUser : null;
                DateTime cbDate = GetSelectedCallbackDate();
                
                var response = agentSession.ConversationSrv.CreateCallback(cbDate, txtPhoneNumber.Text, queueid, agentSession.CurrentUser.Id, callbackUserName);
                Close();
            }

        }
    }
}
