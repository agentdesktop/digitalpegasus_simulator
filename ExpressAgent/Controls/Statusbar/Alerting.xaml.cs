﻿using ExpressAgent.Platform;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for Alerting.xaml
    /// </summary>
    public partial class Alerting : UserControl
    {
        private AgentSession Session
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }

        public string AlertingConversationId = "";
        public Alerting()
        {
            InitializeComponent();
        }
        private void btnDecline_Click(object sender, RoutedEventArgs e)
        {
            Session?.ConversationSrv.ActiveConversation?.AgentParticipant?.Communications?[0].Disconnect();
        }

        private void btnAccept_Click(object sender, RoutedEventArgs e)
        {
            AgentWindow.Instance.AcceptPendingSession(AlertingConversationId);
        }
    }
}
