﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for StatusBar.xaml
    /// </summary>
    public partial class StatusBar : UserControl
    {
        public StatusBar()
        {
            InitializeComponent();
        }

        public void Refresh() 
        {
            //trigger UI update
            var temp = this.DataContext;
            this.DataContext = null;
            this.DataContext = temp;
        }

        private void btnStatusTimer_Click(object sender, RoutedEventArgs e)
        {
            if (btnStatusTimer.IsChecked == true)
            {
                tbStatus.Visibility = Visibility.Collapsed;
                tbDuration.Visibility = Visibility.Visible;
            }
            else 
            {
                tbStatus.Visibility = Visibility.Visible;
                tbDuration.Visibility = Visibility.Collapsed;
            }
        }
        public void SetAlerting(string ConversationId = null) 
        {
            if (ConversationId != null)
            {
                ucAlerting.AlertingConversationId = ConversationId;
                ucAlerting.tbInteraction.Text = "Internal";
                ucAlerting.Visibility = Visibility.Visible;
            }
            else 
            {
                ucAlerting.AlertingConversationId = "";
                ucAlerting.tbInteraction.Text = "";
                ucAlerting.Visibility = Visibility.Collapsed;
            }
        }
    }
}
