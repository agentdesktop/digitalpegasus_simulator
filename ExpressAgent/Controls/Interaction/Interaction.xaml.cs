﻿using System.Windows.Controls;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for Interaction.xaml
    /// </summary>
    public partial class Interaction : UserControl
    {
        public Interaction()
        {
            InitializeComponent();
        }


        private void btnWrapup_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            AgentWindow.Instance.SetAppDialog(AppDialogs.WrapUp);

        }
    }
}
