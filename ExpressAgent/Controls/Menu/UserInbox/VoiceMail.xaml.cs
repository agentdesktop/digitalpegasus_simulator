﻿using ExpressAgent.Platform;
using ExpressAgent.Platform.Models;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for VoiceMail.xaml
    /// </summary>
    public partial class VoiceMail : UserControl
    {
        private AgentSession Session
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }
        public VoiceMail()
        {
            InitializeComponent();
        }


        private void btnVoiceMail_Click(object sender, RoutedEventArgs e)
        {
            ReloadVoicemails();
        }

        async void ReloadVoicemails()
        {
            /*
            List<VoicemailMessage> vms = null;
            await Task.Run(() =>
            {
                vms = Session?.VMSrv.GetVoiceMails();

            });

            //calculate voicemails...
            int vmnum = vms.Where(x => x.Read == false).ToList().Count;
            Session?.VMSrv.UpdateVMCount(vmnum);

            lstVoiceMails.ItemsSource = vms;
            //*/
        }


        MediaPlayer mediaPlayer = new MediaPlayer();

        //https://wpf-tutorial.com/audio-video/playing-audio/
        private void btnVMPlay_Click(object sender, RoutedEventArgs e)
        {
            ExpressVoicemailMessage msg = (sender as Button).Tag as ExpressVoicemailMessage;
            PlayVoicemailMessage(msg);
        }

        async void DownloadVM(ExpressVoicemailMessage msg)
        {
            //non ui thread
            await Task.Run(() =>
            {
                VoicemailMediaInfo info = Session?.VMSrv.GetVoiceMailMedia(msg.Id);
                string dlpath = GetDownloadFolderPath();
                string filename = dlpath + "\\" + msg.Id + ".WEBM";
                using (var client = new WebClient())
                {
                    client.DownloadFile(info.MediaFileUri, filename);
                }
            });
        }

        void PlayVoicemailMessage(ExpressVoicemailMessage msg)
        {

            VoicemailMediaInfo info = Session?.VMSrv.GetVoiceMailMedia(msg.Id);

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(info.MediaImageUri, UriKind.Absolute);
            bitmap.EndInit();
            imgVM.Source = bitmap;

            mediaPlayer.Open(new Uri(info.MediaFileUri, UriKind.Absolute));
            mediaPlayer.Play();

        }



        private void mnuVMAction_Click(object sender, RoutedEventArgs e)
        {
            ExpressVoicemailMessage msg = ((sender as MenuItem).Parent as ContextMenu).Tag as ExpressVoicemailMessage;

            switch ((sender as MenuItem).Tag as string)
            {
                case "Markread":
                    Session?.VMSrv.MarkVoiceMailRead(msg.Id, true);
                    break;
                case "Markunread":
                    Session?.VMSrv.MarkVoiceMailRead(msg.Id, false);
                    break;
                case "Delete":
                    Session?.VMSrv.DeleteVoiceMail(msg.Id);
                    break;
                case "Download":
                    DownloadVM(msg);
                    break;
                case "Callback":
                    break;
                case "Play":
                    PlayVoicemailMessage(msg);
                    break;
            }
        }

        public static string GetHomePath()
        {
            // Not in .NET 2.0
            // System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            if (System.Environment.OSVersion.Platform == System.PlatformID.Unix)
                return System.Environment.GetEnvironmentVariable("HOME");

            return System.Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
        }
        public static string GetDownloadFolderPath()
        {
            if (System.Environment.OSVersion.Platform == System.PlatformID.Unix)
            {
                string pathDownload = System.IO.Path.Combine(GetHomePath(), "Downloads");
                return pathDownload;
            }

            return System.Convert.ToString(
                Microsoft.Win32.Registry.GetValue(
                     @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders"
                    , "{374DE290-123F-4565-9164-39C4925E467B}"
                    , String.Empty
                )
            );
        }
    }
}
