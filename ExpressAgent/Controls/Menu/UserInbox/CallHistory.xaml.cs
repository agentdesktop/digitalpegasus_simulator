﻿using ExpressAgent.Platform;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for CallHistory.xaml
    /// </summary>
    public partial class CallHistory : UserControl
    {
        private AgentSession Session
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }
        public CallHistory()
        {
            InitializeComponent();
        }

        private void btnHistoryReload_Click(object sender, RoutedEventArgs e)
        {
            ReloadHistory();
        }


        async void ReloadHistory()
        {
            Session?.ConversationSrv.SetCallHistoryConversations();
            /*
            //on UI thread
            List<CallHistoryConversation> calls = null;

            //non ui thread
            await Task.Run(() =>
            {
                calls = Session?.ConversationSrv.GetCallHistory();
            });

            //on UI thread
            lstCallHistory.ItemsSource = calls;
            //*/
        }

        private void btnDial_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCRM_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
