﻿using ExpressAgent.Platform;
using ExpressAgent.Platform.Models;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for UserRecordings.xaml
    /// </summary>
    public partial class UserRecordings : UserControl
    {
        private AgentSession Session
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }
        public UserRecordings()
        {
            InitializeComponent();
        }




        MediaPlayer mediaPlayer = new MediaPlayer();

        //https://wpf-tutorial.com/audio-video/playing-audio/
        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            ExpressUserRecording msg = (sender as Button).Tag as ExpressUserRecording;
            PlayUserRecordingsMessage(msg);
        }

        async void DownloadRecording(ExpressUserRecording msg)
        {
            //non ui thread
            await Task.Run(() =>
            {
                DownloadResponse info = Session?.URSrv.GetUserRecordingMedia(msg.Id);
                string dlpath = GetDownloadFolderPath();
                string filename = dlpath + "\\" + msg.Id + ".WEBM";
                using (var client = new WebClient())
                {
                    client.DownloadFile(info.ContentLocationUri, filename);
                }
            });
        }

        void PlayUserRecordingsMessage(ExpressUserRecording msg)
        {

            DownloadResponse info = Session?.URSrv.GetUserRecordingMedia(msg.Id);

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(info.ImageUri, UriKind.Absolute);
            bitmap.EndInit();
            imgUR.Source = bitmap;

            mediaPlayer.Open(new Uri(info.ContentLocationUri, UriKind.Absolute));
            mediaPlayer.Play();

        }



        private void mnuAction_Click(object sender, RoutedEventArgs e)
        {
            ExpressUserRecording msg = ((sender as MenuItem).Parent as ContextMenu).Tag as ExpressUserRecording;

            switch ((sender as MenuItem).Tag as string)
            {
                case "Markread":
                    Session?.URSrv.MarkUserRecordingRead(msg.Id, true);
                    break;
                case "Markunread":
                    Session?.URSrv.MarkUserRecordingRead(msg.Id, false);
                    break;
                case "Delete":
                    //Session?.URSrv.DeleteUserRecordings(msg.Id);
                    break;
                case "Download":
                    DownloadRecording(msg);
                    break;
                case "Callback":
                    break;
                case "Play":
                    PlayUserRecordingsMessage(msg);
                    break;
            }
        }

        public static string GetHomePath()
        {
            // Not in .NET 2.0
            // System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            if (System.Environment.OSVersion.Platform == System.PlatformID.Unix)
                return System.Environment.GetEnvironmentVariable("HOME");

            return System.Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
        }
        public static string GetDownloadFolderPath()
        {
            if (System.Environment.OSVersion.Platform == System.PlatformID.Unix)
            {
                string pathDownload = System.IO.Path.Combine(GetHomePath(), "Downloads");
                return pathDownload;
            }

            return System.Convert.ToString(
                Microsoft.Win32.Registry.GetValue(
                     @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders"
                    , "{374DE290-123F-4565-9164-39C4925E467B}"
                    , String.Empty
                )
            );
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {
            Session?.URSrv.SetUserRecordings();
        }
    }
}
