﻿using ExpressAgent.Platform;
using Microsoft.VisualBasic.ApplicationServices;
using PureCloudPlatform.Client.V2.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using User = PureCloudPlatform.Client.V2.Model.User;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for NewCall.xaml
    /// </summary>
    public partial class NewCall : UserControl
    {
        private AgentSession agentSession
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }

        public static DependencyProperty ParentUserBarProperty = DependencyProperty.Register("ParentUserBar", typeof(MenuBar), typeof(NewCall));

        public MenuBar ParentUserBar
        {
            get { return (MenuBar)GetValue(ParentUserBarProperty); }
            set { SetValue(ParentUserBarProperty, value); }
        }

        public NewCall()
        {
            InitializeComponent();
        }

        private void btnCall_Click(object sender, RoutedEventArgs e)
        {

            string callFromQueueId = null;
            if (chkQueue.IsChecked == true)
            {
                Queue queue = cboQueue.SelectedItem as Queue;
                callFromQueueId = queue.Id;
            }

            if (lstUsers.SelectedItem != null)
            {
                User agent = lstUsers.SelectedItem as User;
                //call agent
                agentSession?.ConversationSrv.CreateCall(Platform.Enums.ConversationTarget.User, agent.Id, callFromQueueId);
            }
            else
            {
                //number
                agentSession?.ConversationSrv.CreateCall(Platform.Enums.ConversationTarget.PhoneNumber, txtTarget.Text, callFromQueueId);
            }
            AgentWindow.Instance.SetAppState(AppStates.Interactions);

            if (ParentUserBar != null)
            {
                //ParentUserBar.NewCallButton.IsChecked = false;
            }
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                txtTarget.Clear();
                txtTarget.Focus();

            }
        }

        async private void txtTarget_TextChanged(object sender, TextChangedEventArgs e)
        {
            string target = txtTarget.Text;
            if (!string.IsNullOrEmpty(target))
            {
                List<User> users = null;
                await Task.Run(() =>
                {
                    users = agentSession?.UsersSrv.SearchTargets(target);
                });

                lstUsers.ItemsSource = users;
            }
            else 
            {
                lstUsers.ItemsSource = null;
            }
        }
    }
}
