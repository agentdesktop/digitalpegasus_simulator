﻿using ExpressAgent.Platform;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for QueueActivation.xaml
    /// </summary>
    public partial class QueueActivation : UserControl
    {


        private AgentSession Session
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }
        public QueueActivation()
        {
            InitializeComponent();
            this.IsVisibleChanged += UserControl_IsVisibleChanged;

        }
        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                Refresh();
            }
        }

        async private void Refresh() 
        {

            List<UserQueue> myQueues = await Task.Run(() =>
            {
                return Session?.RoutingSrv.GetMyQueues();
            });
            //List<UserQueue> availableQueues = AgentSession?.RoutingSrv.GetMyQueues(1, false);

            lstActive.ItemsSource = myQueues.Where(x => x.Joined == true).ToList();
            lstAvailable.ItemsSource = myQueues.Where(x => x.Joined == false).ToList();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        async private void btnActivate_Click(object sender, RoutedEventArgs e)
        {
            UserQueue queue = (sender as Button).Tag as UserQueue;

            UserQueue result = await Task.Run(() =>
            {
                return Session?.UsersSrv.ActivateMyQueue(queue, true);
            });

            Refresh();
        }
        async private void btnDeactivate_Click(object sender, RoutedEventArgs e)
        {
            UserQueue queue = (sender as Button).Tag as UserQueue;
            UserQueue result = await Task.Run(() =>
            {
                return Session?.UsersSrv.ActivateMyQueue(queue, false);
            });
            Refresh();
        }

        

    }
}
