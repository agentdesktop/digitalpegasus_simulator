﻿using ExpressAgent.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : UserControl
    {
        private AgentSession Session
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }
        public Settings()
        {
            InitializeComponent();
            cboMic.ItemsSource = AgentWindow.Instance.MicDevices;
            cboRng.ItemsSource = AgentWindow.Instance.RingDevices;
            cboSpk.ItemsSource = AgentWindow.Instance.RingDevices;
        }



        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            exp1.IsExpanded = sender == exp1;
            exp2.IsExpanded = sender == exp2;
            exp3.IsExpanded = sender == exp3;
            exp4.IsExpanded = sender == exp4;
            exp1.Visibility = sender == exp1 ? Visibility.Visible : Visibility.Collapsed;
            exp2.Visibility = sender == exp2 ? Visibility.Visible : Visibility.Collapsed;
            exp3.Visibility = sender == exp3 ? Visibility.Visible : Visibility.Collapsed;
            exp4.Visibility = sender == exp4 ? Visibility.Visible : Visibility.Collapsed;
            tbSettings.Visibility = Visibility.Collapsed;
            tbBack.Visibility = Visibility.Visible;
        }

        private void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            exp1.Visibility =  Visibility.Visible ;
            exp2.Visibility =  Visibility.Visible ;
            exp3.Visibility =  Visibility.Visible ;
            exp4.Visibility =  Visibility.Visible ;
            tbSettings.Visibility = Visibility.Visible;
            tbBack.Visibility = Visibility.Collapsed;

        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (exp1.IsExpanded) exp1.IsExpanded = false;
            if (exp2.IsExpanded) exp2.IsExpanded = false;
            if (exp3.IsExpanded) exp3.IsExpanded = false;
            if (exp4.IsExpanded) exp4.IsExpanded = false;

        }

        private void btnWebRTCAdvancedBack_Click(object sender, RoutedEventArgs e)
        {
            stpWebRTCAdvanced.Visibility = Visibility.Collapsed;
            stpWebRTCMain.Visibility = Visibility.Visible;
        }

        private void btnAdvMic_Click(object sender, RoutedEventArgs e)
        {
            stpWebRTCAdvanced.Visibility = Visibility.Visible;
            stpWebRTCMain.Visibility = Visibility.Collapsed;
        }
    }
}
