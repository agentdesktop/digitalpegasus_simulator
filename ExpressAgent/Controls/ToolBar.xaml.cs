﻿using ExpressAgent.Platform.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for ToolBar.xaml
    /// </summary>
    public partial class ToolBar : UserControl
    {
        public ToolBar()
        {
            InitializeComponent();
        }

        private void btnToolbar_Click(object sender, RoutedEventArgs e)
        {
            ExpressConversationParticipantCall call = tbarMain.DataContext as ExpressConversationParticipantCall;
            if (call != null)
            {
                string sTag = (sender as Button) != null ? (sender as Button).Tag.ToString() : (sender as ToggleButton).Tag.ToString();
                switch (sTag)
                {
                    case "PickUp":
                        //call.Pickup();
                        //use webrtc to accept the call
                        AgentWindow.Instance.AcceptPendingSession(call.Participant.Conversation.Id);
                        break;
                    case "Disconnect":
                        call.Disconnect();
                        break;
                    case "Hold":
                        call.ToggleHold();
                        break;
                    case "Mute":
                        call.ToggleMute();
                        break;
                    case "Record":
                        call.ToggleRecording();
                        break;
                    case "SecurePause":
                        call.ToggleSecurePaused();
                        break;
                    case "Flag":
                        call.ToggleFlagged();
                        break;

                    case "ACW":
                        call.ToggleAftercallwork();
                        break;

                    case "Transfer":
                        AgentWindow.Instance.SetAppDialog(AppDialogs.Transfer);
                        break;
                    case "CallBack":
                        AgentWindow.Instance.SetAppDialog(AppDialogs.CallBack);
                        break;
                    case "DTMF":
                        AgentWindow.Instance.SetAppDialog(AppDialogs.DTMF);
                        break;

                    case "Recordings":
                        //AgentWindow.Instance.SetAppDialog(AppDialogs.Recordings);
                        break;
                }
            }
        }

    }
}
