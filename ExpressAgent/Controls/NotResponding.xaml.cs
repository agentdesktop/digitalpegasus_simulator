﻿using ExpressAgent.Platform;
using ExpressAgent.Platform.Models;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for NotResponding.xaml
    /// </summary>
    public partial class NotResponding : UserControl
    {
        private bool _isLoaded;

        private AgentSession Session
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }

        public NotResponding()
        {
            InitializeComponent();
            this.Unloaded += NotResponding_Unloaded;
            this.DataContextChanged += NotResponding_DataContextChanged;

        }


        private void OnQueueButton_Click(object sender, RoutedEventArgs e)
        {
            Session?.UsersSrv.SetUserIdle();
        }

        private void OffQueueButton_Click(object sender, RoutedEventArgs e)
        {
            ExpressPresence offQueue = Session?.PresenceSrv.OrgPresences.Where(p => p.SystemPresence == "Available" && p.Primary == true).FirstOrDefault();

            if (offQueue == null)
            {
                Debug.WriteLine("NotResponding: Off Queue presence not found.");
                return;
            }

            Session?.PresenceSrv.SetUserPresence(offQueue.Id);
        }

        private void User_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "CurrentRoutingState")
            {
                return;
            }

            if (Session?.UsersSrv.CurrentRoutingState == Platform.Enums.RoutingState.NotResponding)
            {
                Visibility = Visibility.Visible;
            }
            else
            {
                Visibility = Visibility.Collapsed;
            }
        }

        private void NotResponding_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (_isLoaded || Session == null)
            {
                return;
            }

            Session.UsersSrv.PropertyChanged += User_PropertyChanged;

            if (Session?.UsersSrv.CurrentRoutingState == Platform.Enums.RoutingState.NotResponding)
            {
                Visibility = Visibility.Visible;
            }

            _isLoaded = true;
        }

        private void NotResponding_Unloaded(object sender, RoutedEventArgs e)
        {
            if (!_isLoaded)
            {
                return;
            }

            if (Session != null)
            {
                Session.UsersSrv.PropertyChanged -= User_PropertyChanged;
            }

            _isLoaded = false;
        }
    }
}
