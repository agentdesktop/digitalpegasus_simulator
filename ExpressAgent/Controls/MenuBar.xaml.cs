﻿using ExpressAgent.Platform;
using System.Windows.Controls;

namespace ExpressAgent.Controls 
{ 
    /// <summary>
    /// Interaction logic for MenuBar.xaml
    /// </summary>
    public partial class MenuBar : UserControl
    {
        private AgentSession Session
        {
            get
            {
                return AgentWindow.Instance.agentSession as AgentSession;
            }
        }

        public MenuBar()
        {
            InitializeComponent();
        }

        private void PresenceComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Session?.PresenceSrv.SetUserPresence((sender as ComboBox).SelectedValue as string, Session?.PresenceSrv.CurrentPresence.Message);
            Session?.PresenceSrv.SetUserPresence((sender as ComboBox).SelectedValue as string);
        }

        private void LogoutMenuItem_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Session?.Logout();
        }

        private void UserStackPanel_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            UserStackPanel.ContextMenu.IsOpen = true;
        }

        private void HamburgerMenuItem_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            switch ((sender as MenuItem).Name) 
            {
                case "Interactions":
                    AgentWindow.Instance.SetAppState(AppStates.Interactions);
                    break;
                case "NewInteraction":
                    AgentWindow.Instance.SetAppState(AppStates.NewCall);
                    break;
                case "UserInbox":
                    AgentWindow.Instance.SetAppState(AppStates.UserInbox);
                    break;
                case "QueueActivation":
                    AgentWindow.Instance.SetAppState(AppStates.QueueActivation);
                    break;
                case "Settings":
                    AgentWindow.Instance.SetAppState(AppStates.Settings);
                    break;
            }

        }



        private void lstPresence_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Session?.PresenceSrv.SetUserPresence((sender as ListBox).SelectedValue as string);
            presenceDropdown.IsOpen = false;
        }

        private void btnLogout_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Session?.Logout();
        }
    }
}
