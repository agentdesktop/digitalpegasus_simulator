﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for BurstedImage.xaml
    /// </summary>
    public partial class BurstedImage : UserControl
    {
        public BurstedImage()
        {
            InitializeComponent();
            OnBurstCountChanged(new DependencyPropertyChangedEventArgs(BurstCountProperty, 0, 0));
        }

        #region BurstCount

        //register attached dependency property
        public static readonly DependencyProperty BurstCountProperty =
         DependencyProperty.Register("BurstCount", typeof(int), typeof(BurstedImage), new
            PropertyMetadata(0, new PropertyChangedCallback(OnBurstCountChangedCallback)));

        public int BurstCount
        {
            get { return (int)GetValue(BurstCountProperty); }
            set { SetValue(BurstCountProperty, value); }
        }

        private static void OnBurstCountChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BurstedImage uc = d as BurstedImage;
            uc.OnBurstCountChanged(e);
        }

        #endregion
        private void OnBurstCountChanged(DependencyPropertyChangedEventArgs e)
        {
            int _BurstCount = (int)e.NewValue;
            txtBurstCount.Text = "" + _BurstCount;
            txtBurstCountShadow.Text = "" + _BurstCount;
            txtBurstCount.Visibility = _BurstCount > 0 ? Visibility.Visible : Visibility.Collapsed;
            txtBurstCountShadow.Visibility = _BurstCount > 0 ? Visibility.Visible : Visibility.Collapsed;
            IsBursted = _BurstCount > 0;
            imgBursted.Visibility = IsBursted == true ? Visibility.Visible : Visibility.Collapsed;
        }

        bool _IsBursted = false;
        public bool IsBursted { 
            get { return _IsBursted; } 
            set { 
                if (_IsBursted != value)
                {
                    _IsBursted = value; 
                    imgBursted.Visibility = _IsBursted == true ? Visibility.Visible : Visibility.Collapsed;
                }
            } }

        string _Source;
        public string Source 
        { 
            get { return _Source; } 
            set 
            {
                if (_Source != value)
                {
                    _Source = value;

                    // Create source
                    BitmapImage myBitmapImage = new BitmapImage();

                    // BitmapImage.UriSource must be in a BeginInit/EndInit block
                    myBitmapImage.BeginInit();

                    var uri = new Uri(Directory.GetCurrentDirectory() + value, UriKind.Absolute);
                    //myBitmapImage.UriSource = new Uri(@"C:\Documents and Settings\All Users\Documents\My Pictures\Sample Pictures\Water Lilies.jpg");
                    myBitmapImage.UriSource = uri;
                    // To save significant application memory, set the DecodePixelWidth or
                    // DecodePixelHeight of the BitmapImage value of the image source to the desired
                    // height or width of the rendered image. If you don't do this, the application will
                    // cache the image as though it were rendered as its normal size rather than just
                    // the size that is displayed.
                    // Note: In order to preserve aspect ratio, set DecodePixelWidth
                    // or DecodePixelHeight but not both.
                    //myBitmapImage.DecodePixelWidth = 16;
                    myBitmapImage.EndInit();

                    //var bmimage = new BitmapImage(new Uri(value, UriKind.Relative));
                    imgNormal.Source = myBitmapImage;
                }

            } }

        /*
        #region image prop
        //register attached dependency property
        public static readonly DependencyProperty ImageProperty = DependencyProperty.RegisterAttached("Image",
                                                                typeof(ImageSource),
                                                                typeof(BurstedImage), 
                                                                new FrameworkPropertyMetadata((ImageSource)null)
            );

        /// <summary>
        /// Gets the <see cref="ImageProperty"/> for a given
        /// <see cref="DependencyObject"/>, which provides an
        /// <see cref="ImageSource" /> for arbitrary WPF elements.
        /// </summary>
        public static ImageSource GetImage(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(ImageProperty);
        }

        /// <summary>
        /// Sets the attached <see cref="ImageProperty"/> for a given
        /// <see cref="DependencyObject"/>, which provides an
        /// <see cref="ImageSource" /> for arbitrary WPF elements.
        /// </summary>
        public static void SetImage(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(ImageProperty, value);
        }
        #endregion

        #region Bursted

        //register attached dependency property
        public static readonly DependencyProperty BurstedProperty =
         DependencyProperty.Register("Bursted", typeof(bool), typeof(BurstedImage), new
            PropertyMetadata(false, new PropertyChangedCallback(OnBurstedChangedCallback)));

        public bool Bursted
        {
            get { return (bool)GetValue(BurstedProperty); }
            set { SetValue(BurstedProperty, value); }
        }

        private static void OnBurstedChangedCallback(DependencyObject d,DependencyPropertyChangedEventArgs e)
        {
            BurstedImage uc = d as BurstedImage;
            uc.OnBurstedChanged(e);
        }

        #endregion
        private void OnBurstedChanged(DependencyPropertyChangedEventArgs e)
        {
            bool IsBursted = (bool)e.NewValue;
            if (IsBursted)
            {

            }
        }
        //*/
    }
}
