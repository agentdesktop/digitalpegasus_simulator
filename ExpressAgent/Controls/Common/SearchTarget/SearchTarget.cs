﻿using PureCloudPlatform.Client.V2.Model;

namespace ExpressAgent.Controls
{
    public class SearchTarget : BindableBase
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private User _user;
        public User User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private Queue _queue;
        public Queue Queue
        {
            get { return _queue; }
            set { SetProperty(ref _queue, value); }
        }

        private TargetTypes _type;
        public TargetTypes Type
        {
            get { return _type; }
            set { SetProperty(ref _type, value); }
        }

        public override string ToString()
        {
            return $"{Name} - {Type}";
        }
    }
}
