﻿using System.Windows;
using System.Windows.Controls;

namespace ExpressAgent.Controls
{
    public class TargetTemplateSelector : DataTemplateSelector
    {
        public DataTemplate UserTemplate { get; set; }

        public DataTemplate QueueTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var selectedTemplate = this.UserTemplate;

            var target = item as SearchTarget;
            if (target == null) return selectedTemplate;

            switch (target.Type)
            {
                case TargetTypes.User:
                    selectedTemplate = this.UserTemplate;
                    break;

                case TargetTypes.Queue:
                    selectedTemplate = this.QueueTemplate;
                    break;
            }
            return selectedTemplate;
        }
    }
}
