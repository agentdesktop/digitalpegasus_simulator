﻿using NLog.Targets;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressAgent.Controls
{
    /// <summary>
    /// Interaction logic for SearchTargetResult.xaml
    /// </summary>
    public partial class SearchTargetResult : UserControl
    {
        public SearchTargetResult()
        {
            InitializeComponent();
        }

        public IEnumerable ItemsSource { get { return lstMain.ItemsSource; } set { lstMain.ItemsSource = value; } }
        public object SelectedItem { get { return lstMain.SelectedItem;} set { lstMain.SelectedItem = value; } }
    }
}
