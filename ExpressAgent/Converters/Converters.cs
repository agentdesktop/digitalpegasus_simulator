﻿using PureCloudPlatform.Client.V2.Model;
using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace ExpressAgent.Converters
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BooleanVisibilityConverterMulti : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility vis = Visibility.Collapsed;

            if (!(values[0] is bool value))
            {
                return null;
            }

            if (value)
            {
                vis = Visibility.Visible;
            }

            return vis;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(bool?), typeof(Visibility))]
    public class BooleanVisibilityConverter : IValueConverter
    {
        public object Convert(object ovalue, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility vis = Visibility.Collapsed;

            if (!(ovalue is bool bvalue))
            {
                return null;
            }

            if (parameter != null && (string)parameter == "false")
            {
                vis = bvalue == false ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                vis = bvalue==true ? Visibility.Visible: Visibility.Collapsed;
            }

            return vis;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(int?), typeof(string))]
    public class DurationSecConverter : IValueConverter
    {
        public object Convert(object ovalue, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            string duration = "";
            if (!(ovalue is int ivalue))
            {
                return duration;
            }
            TimeSpan interval = TimeSpan.FromSeconds((double)ivalue);
            duration = interval.ToString(@"hh\:mm\:ss");
            return duration;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(DateTime?), typeof(string))]
    public class VMDateConverter : IValueConverter
    {
        public object Convert(object ovalue, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string sdate = "";
            if (!(ovalue is DateTime dtvalue))
            {
                return sdate;
            }
            sdate = dtvalue.ToString("dd/MM/yyyy hh:mm:ss tt");
            return sdate;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(bool?), typeof(SolidColorBrush))]
    public class ReadColorConverter : IValueConverter
    {
        SolidColorBrush falseBrush = new SolidColorBrush(Colors.Red);
        SolidColorBrush trueBrush = new SolidColorBrush(Colors.Black);
        public object Convert(object ovalue, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(ovalue is bool bvalue))
            {
                return falseBrush;
            }
            return bvalue==true ? trueBrush : falseBrush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(CallHistoryParticipant), typeof(string))]
    public class CallHistoryParticipantConverter : IValueConverter
    {
        public object Convert(object ovalue, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(ovalue is CallHistoryParticipant participant))
            {
                return "?";
            }
            switch (participant.Purpose) 
            {
                case "acd":
                case "queue":
                    return participant.Queue.Name;
                case "agent":
                case "user":
                    return participant.User.Name;
                case "external":
                case "customer":
                case "ivr":
                default:
                    return participant.Purpose;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //[ValueConversion(typeof(string), typeof(SolidColorBrush))]
    public class PresenceColorConverter : IValueConverter
    {

        readonly SolidColorBrush OfflineBrush = new SolidColorBrush(Colors.Black);

        public object Convert(object ovalue, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (ovalue == null) return OfflineBrush;
            string SystemPresence = (string)ovalue;
            switch (SystemPresence) 
            {
                case "Available":
                    return new SolidColorBrush(Colors.Green); 
                case "Meeting":
                case "Busy":
                    return new SolidColorBrush(Colors.Red);
                case "Idle":
                    return new SolidColorBrush(Colors.Gray);
                case "On Queue":
                    return new SolidColorBrush(Colors.Blue);
                case "Offline":
                    return OfflineBrush;
                default:
                    return new SolidColorBrush(Colors.Orange);
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(DateTime?), typeof(string))]
    public class PresenceDurationConverter : IValueConverter
    {

        string FormatDuration(TimeSpan duration)
        {
            if (duration.TotalSeconds == 0)
            {
                return "";
            }
            else if (duration.Days > 0)
            {
                return duration.ToString("%d") + "d " + duration.ToString("%h") + "h " + duration.ToString("%m") + "m " + duration.ToString("%s") + "s";
            }
            else if (duration.Hours > 0)
            {
                return duration.ToString("%h") + "h " + duration.ToString("%m") + "m " + duration.ToString("%s") + "s";
            }
            else if (duration.Minutes > 0)
            {
                return duration.ToString("%m") + "m " + duration.ToString("%s") + "s";
            }
            else
            {
                return duration.ToString("%s") + "s";
            }
        }
        public object Convert(object ovalue, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string duration = "";
            if (!(ovalue is DateTime dtValue))
            {
                return duration;
            }
            return FormatDuration(DateTime.UtcNow - dtValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
