﻿using ExpressAgent.Auth;
using ExpressAgent.Platform;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Web.WebView2.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Resources;
using System.Windows.Shapes;

namespace ExpressAgent
{

    public class WebDevice 
    {
        public string deviceId { get; set; }
        public string kind { get; set; }
        public string label { get; set; }
        public string groupId { get; set; }
    }

    /// <summary>
    /// Interaction logic for WebRtcHost.xaml
    /// </summary>
    public partial class WebRtcHost : UserControl
    {

        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();
        private readonly string HostUrl = "http://localhost:7070";
        private IHost _host;
        bool bStartSDK = true;
        private AgentSession agentSession
        {
            get
            {
                return DataContext as AgentSession;
            }
        }
        public WebRtcHost()
        {
            InitializeComponent();
            this.Loaded += WebRtcHost_Loaded;
        }

        private void WebRtcHost_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= WebRtcHost_Loaded;
            if (bStartSDK == true)
            {
                InitWebrtc();
            }
        }


        public void InitWebrtc() 
        { 
            try
            {
                _host = Host.CreateDefaultBuilder()
                               .ConfigureWebHostDefaults(webHostBuilder =>
                               {
                                   webHostBuilder.UseStartup<Startup>().UseUrls(HostUrl).UseKestrel().UseContentRoot(Directory.GetCurrentDirectory());
                               })
                               .Build();

                _host.Start();
                InitializeAsync();
            }
            catch (Exception ex)
            {
                Log.Error("WebRtcHost_Loaded error" + ex);
            }
        }


        async void InitializeAsync()
        {
            await webView.EnsureCoreWebView2Async(null);
            webView.CoreWebView2.WebMessageReceived += CoreWebView2_WebMessageReceived; ;
            webView.CoreWebView2.DOMContentLoaded += CoreWebView2_DOMContentLoaded;
            webView.Source = new Uri(HostUrl + "/sdkhost.html");
        }

        private void CoreWebView2_DOMContentLoaded(object sender, CoreWebView2DOMContentLoadedEventArgs e)
        {
            /*
            dynamic obj = new System.Dynamic.ExpandoObject();
            obj.desc = "test";
            obj.qta = 1;
            obj.prezzo = 2;
            string data = JsonConvert.SerializeObject(obj);
            //*/
            webView.ExecuteScriptAsync( string.Format("initSdk('{0}','{1}')", AuthSession.Current.AuthToken, AuthSession.Current.Environment) );
        }

        public void AcceptPendingSession(string conversationId,string micDeviceId) 
        {
            webView.ExecuteScriptAsync(string.Format("acceptPendingSession('{0}','{1}')", conversationId, micDeviceId));
        }

        private void CoreWebView2_WebMessageReceived(object? sender, CoreWebView2WebMessageReceivedEventArgs e)
        {
            try
            {
                string jsonString = e.WebMessageAsJson;
                if (jsonString != null)
                {
                    AgentWindow.Instance.LogMessage("WebMessageReceived:" + jsonString);
                    return;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(jsonString);
                    string eventname = dynamicObject.eventname;
                    dynamic body = dynamicObject.body;
                    switch (eventname)
                    {
                        case "ready":
                            AgentWindow.Instance.LogMessage("WebRtc ready");
                            var availablePresence = agentSession.PresenceSrv.OrgPresences.Where(x => x.SystemPresence == "Available").FirstOrDefault();
                            if (agentSession.PresenceSrv.CurrentPresence.Id != availablePresence.Id) 
                            {
                                //agentSession.PresenceSrv.SetUserPresence(availablePresence.Id, agentSession.PresenceSrv.CurrentPresence.Message);
                            }

                            break;
                        case "sdkError":
                            AgentWindow.Instance.LogMessage("WebRtc sdkError:" + body);
                            break;
                        case "pendingSession":
                            AgentWindow.Instance.LogMessage("WebRtc pendingSession:" + body.conversationId);
                            break;
                        default:
                            AgentWindow.Instance.LogMessage("WebRtc "+ eventname + ":" + body);
                            break;
                    }
                }
            }
            catch (Exception ex) 
            {
                AgentWindow.Instance.LogMessage("CoreWebView2_WebMessageReceived error:" + ex.Message);
            }
        }

        public void Destory() 
        {
            _host?.Dispose();
            webView?.Dispose();
        }


    }
}
