const http = require('http'); // Import Node.js core module
const fs = require('fs')
const host = 'localhost';
const port = 8000;

const requestListener = function (req, res) {   //create web server

    if (req.url == '/') { //check the URL of the current request
        
        // set response header
        res.writeHead(200, { 'Content-Type': 'text/html' }); 
        
        // set response content    
		fs.createReadStream('sdkhost.html').pipe(res)
        //res.write('<html><body><p>This is home Page.</p></body></html>');res.end();
    
    }
    else if (req.url == "/student") {
        
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write('<html><body><p>This is student Page.</p></body></html>');
        res.end();
    
    }
    else{
        res.end('Invalid Request!');
	}

};

const server = http.createServer(requestListener);
server.listen(port, host, (err) => { 
	if (err) {
		return console.log('something bad happened', err)
	}

    console.log(`Server is running on http://${host}:${port}`);
});



