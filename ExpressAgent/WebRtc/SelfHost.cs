﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace ExpressAgent
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();
            app.UseCors();
            app.UseEndpoints(routes =>
            {
                routes.MapControllers();
            });
        }
    }

    public class HomeController : Controller
    {

        string GetFileContent()
        {
            string cuurDir = System.IO.Directory.GetCurrentDirectory();
            string path = cuurDir + "\\WebRtc\\sdkhost.html";
            string content = System.IO.File.ReadAllText(path);
            return content;
        }

        [HttpGet("/sdkhost.html")]
        public ContentResult Index()
        {
            string html = GetFileContent();
            return base.Content(html, "text/html");
        }


    }
}
