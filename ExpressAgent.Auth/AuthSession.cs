﻿using Microsoft.Extensions.Configuration;
using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;

namespace ExpressAgent.Auth
{
    public class AuthSession : INotifyPropertyChanged
    {
        private NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        public AuthSession() 
        {
            try
            {
                string currDir = Directory.GetCurrentDirectory();
                IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(currDir).AddJsonFile("appSettings.json").Build();
                ClientId = configuration["ClientId"];
                Environment = configuration["Environment"];
                RedirectUri = configuration["RedirectUri"];

            }
            catch (Exception ex)
            {
                Log.Error("AuthSession error:" + ex);
            }
        }
        private static AuthSession _Current;
        public static AuthSession Current
        {
            get
            {
                if (_Current == null)
                {
                    _Current = new AuthSession();
                }

                return _Current;
            }
        }


        public string ClientId = "";
        public string Environment = "";
        public string RedirectUri = "";


        private string _AuthToken;
        public string AuthToken
        {
            get
            {
                return _AuthToken;
            }
            set
            {
                if (value != _AuthToken)
                {
                    _AuthToken = value;
                    OnPropertyChanged();
                }
            }
        }


        public bool HasToken => !string.IsNullOrEmpty(AuthToken);



        public void Authenticate(bool ForceLoginPrompt = false)
        {
            ShowLoginWindow(false, ForceLoginPrompt);
        }

        public void Logout()
        {
            ShowLoginWindow(true);
        }

        private void ShowLoginWindow(bool logout,bool ForceLoginPrompt = false)
        {
            AuthToken = "";
            LoginWindow window = new LoginWindow(this, logout, ForceLoginPrompt);
            window.Show();
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
