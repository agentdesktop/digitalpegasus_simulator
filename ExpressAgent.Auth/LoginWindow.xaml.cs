﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;
using GenesysCloudOAuthWebView.Core;
using GenesysCloudOAuthWebView.WinForms;
using Microsoft.Extensions.Configuration;
using Clipboard = System.Windows.Clipboard;
//using GenesysCloudOAuthWebView.Wpf;


namespace ExpressAgent.Auth
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private bool QuitOnClose = true;
        private OAuthWebView AuthBrowser;
        private AuthSession authSession;
        private bool IsAuthenticating;
        private bool PerformLogout;

        public LoginWindow(AuthSession session, bool performLogout, bool ForceLoginPrompt = false)
        {
            InitializeComponent();
            authSession = session;
            PerformLogout = performLogout;

            SetupBrowser(ForceLoginPrompt);
        }

        public void Login()
        {
            Debug.WriteLine("LoginWindow: Logging in...");

            IsAuthenticating = true;
            BrowserHost.Visibility = Visibility.Visible;
            AuthBrowser?.BeginImplicitGrant();
        }

        public void Logout()
        {
            Debug.WriteLine("LoginWindow: Logging out...");

            IsAuthenticating = false;
            BrowserHost.Visibility = Visibility.Collapsed;

            //AuthBrowser.Navigate($"https://login.{Environment}/logout?client_id={ClientId}&redirect_uri={RedirectUri}");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (PerformLogout)
            {
                Logout();
            }
            else
            {
                Login();
            }
        }

        private void SetupBrowser(bool ForceLoginPrompt = false)
        {

            /*
            var oAuthWebView = new OAuthWebView();
            oAuthWebView.Config.Environment = Environment;
            oAuthWebView.Config.ClientId = ClientId;
            oAuthWebView.Config.RedirectUriIsFake = true;
            oAuthWebView.Config.RedirectUri = RedirectUri;
            oAuthWebView.Config.ForceLoginPrompt = true;
            var result = oAuthWebView.Authenticated//*/

            AuthBrowser = new OAuthWebView
            {
                Dock = DockStyle.Fill
            };

          
            AuthBrowser.Config.Environment = AuthSession.Current.Environment;
            AuthBrowser.Config.ClientId = AuthSession.Current.ClientId;
            AuthBrowser.Config.RedirectUriIsFake = true;
            AuthBrowser.Config.RedirectUri = AuthSession.Current.RedirectUri;
            AuthBrowser.Config.ForceLoginPrompt = ForceLoginPrompt;
            //AuthBrowser.Config.ForceLoginPrompt = true;

            //AuthBrowser.Config.Org = "";


            AuthBrowser.Authenticated += AuthBrowser_Authenticated;
            AuthBrowser.ExceptionEncountered += AuthBrowser_ExceptionEncountered;
            //AuthBrowser.Navigated += AuthBrowser_Navigated;
            //AuthBrowser.Navigating += AuthBrowser_Navigating;

            Panel panel = new Panel { Dock = DockStyle.Fill };
            panel.Controls.Add(AuthBrowser);
            BrowserHost.Child = panel;
        }

        private void AuthBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (e.Url.ToString().Contains(AuthSession.Current.RedirectUri))
            {
                BrowserHost.Visibility = Visibility.Hidden;
            }
            else
            {
                BrowserHost.Visibility = Visibility.Visible;
            }
        }

        private void AuthBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            if (e.Url.ToString().Contains(AuthSession.Current.RedirectUri) && !IsAuthenticating)
            {
                Login();
            }
        }

        private void AuthBrowser_ExceptionEncountered(string source, Exception ex)
        {
            System.Windows.MessageBox.Show("There was a problem signing in to Genesys Cloud. Please try again.\r\n" + ex.Message);
            Login();
        }

        private void AuthBrowser_Authenticated(OAuthResponse response)
        {
            if (!string.IsNullOrEmpty(response.AccessToken))
            {
                Debug.WriteLine("LoginWindow: Successfully obtained access token");
                authSession.AuthToken = response.AccessToken;
#if DEBUG
                Clipboard.SetText(authSession.AuthToken);
#endif
                QuitOnClose = false;
                Close();
            }
            else 
            {
                System.Windows.MessageBox.Show("Auth error!");
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (QuitOnClose)
            {
                System.Windows.Application.Current.Shutdown();
            }
        }
    }

}
