# Embeddable AgentDesktop

An example basic agent UI application using the Genesys Cloud SDK for .NET.

## Supported Features

- Multo region login
- Presence and routing status control
- Place outbound calls
- Receive inbound calls
  - Pickup
  - Hold
  - Mute
  - Disconnect
  - Wrap-up
- Handle multiple interactions
- WebRTC

## Unsupported Features

- Transfers
- Optional wrap-up
- Non-voice interactions
